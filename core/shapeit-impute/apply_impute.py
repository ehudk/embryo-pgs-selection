import multiprocessing
import subprocess
import os
import shutil
import glob
import tarfile
import pandas as pd

if os.name == "nt":
    DATA_DIR = r"C:\Studies\MSc\carmi_thesis\research\data"
    IMPUTE_EXE_PATH = ""
elif os.name == "posix":
    DATA_DIR = os.path.join(os.sep, "cs", "icore", "ehudk", "carmi_thesis", "data")
    IMPUTE_EXE_PATH = os.path.join(os.sep, "cs", "usr", "ehudk", "safe", "carmi_thesis", "toolsets",
                                   "impute", "impute2")
else:
    raise EnvironmentError

# File names:
GENETIC_MAP_BASE_NAME = "genetic_map_chr{}_combined_b37.txt"
HAPLOTYPE_BASE_NAME = "gwas_autosomal.chr{:02}.{}"
IMPUTE_OUTPUT_BASE_NAME = "gwas_autosomal.chr{:02}.impute2"
# IMPUTE_CHUNK_OUTPUT_BASE_NAME = "gwas_autosomal.chr{:02}.chunk{}-{}.impute2"
IMPUTE_CHUNK_OUTPUT_BASE_NAME = "gwas_autosomal.chr{:02}.chunk{:02}.impute2"
REF_PANEL_BASE_NAME = "{}.merged.snp_indel.{}"

# Files location:
GWAS_DIR = os.path.join(DATA_DIR, "cent_kids")
HAPLOTYPE_PATH = os.path.join(GWAS_DIR, "haplotypes", HAPLOTYPE_BASE_NAME)
IMPUTE_DIR_PATH = os.path.join(GWAS_DIR, "impute_shrink")
GENETIC_MAP_PATH = os.path.join(DATA_DIR, "genetic_map_b37", GENETIC_MAP_BASE_NAME)
REF_PANEL_DIR_PATH = os.path.join(DATA_DIR, "tagc_reference_panel")

CHUNK_SIZE = int(5e6)  # 5Mb


def concat_impute_output_chunks(input_file_names, output_file_name, header="keep", engine="python", to_gzip=True):
    """

    Parameters
    ----------
    input_file_names: list
        list of file names in IMPUTE_DIR. Will be concatenated by the order of the list!
    output_file_name
    header: {"keep", "ignore", "keep_first", "comment"}
        What to do with the first line of the files: (only applicable with method="python".
        - "keep" - keeps it as is.
        - "ignore" - ignores it entirely for all files.
        - "keep_first" - keeps only the header from the first file and ignores for the rest of the files.
        - "comment": comments out using '#'.
    engine: {"python", "linux"}
        Whether to use python's iternal tools or to call on unix shell. (unix might be faster)
    to_gzip: bool
        whether to tar-gz the resulted file

    Returns
    -------

    """
    if engine == "python":
        with open(os.path.join(IMPUTE_DIR_PATH, output_file_name), "w") as output_fh:
            for i, input_file_name in enumerate(input_file_names):
                with open(os.path.join(IMPUTE_DIR_PATH, input_file_name), "r") as input_fh:
                    if header == "comment":
                        output_fh.write("#" + next(input_fh))
                    elif header == "keep":
                        pass
                    elif header == "keep_first":
                        if i == 0:
                            output_fh.write(next(input_fh))
                    elif header == "ignore":
                        next(input_fh)
                    else:
                        raise ValueError("header option ({}) is not a valid one".format(header))

                    for line in input_fh:
                        output_fh.write(line)
    elif engine == "linux":
        with open(os.path.join(IMPUTE_DIR_PATH, output_file_name), "w") as output_fh:
            subprocess.run(["cat"] +
                           [os.path.join(IMPUTE_DIR_PATH, input_file_name) for input_file_name in input_file_names],
                           stdout=output_fh)
    else:
        raise ValueError("Unsupported engine ({})".format(engine))
    if to_gzip:
        with tarfile.open(os.path.join(IMPUTE_DIR_PATH, output_file_name) + ".tar.gz", "w:gz") as tar_fh:
            tar_fh.add(os.path.join(IMPUTE_DIR_PATH, output_file_name), arcname=output_file_name)


def run_impute2_single_thread(chr_num, chunks_positions):
    for i in range(len(chunks_positions) - 1):
        low_edge = chunks_positions[i]
        high_edge = chunks_positions[i + 1]
        impute2_single_call(chr_num, i, low_edge, high_edge)


def run_impute2_multi_thread(chr_num, chunks_positions):
    chunk_ids = list(range(len(chunks_positions) - 1))
    low_edges = chunks_positions[:-1]
    high_edges = chunks_positions[1:]
    chr_nums = [chr_num] * len(chunk_ids)
    arguments = zip(chr_nums, chunk_ids, low_edges, high_edges)

    pool = multiprocessing.Pool()
    pool.starmap(impute2_single_call, arguments)
    pool.close()
    pool.join()


def impute2_single_call(chr_num, chunk_num, low_edge, high_edge):
    subprocess.run([IMPUTE_EXE_PATH,
                    "-known_haps_g", HAPLOTYPE_PATH.format(chr_num, "haps"),
                    "-m", GENETIC_MAP_PATH.format(chr_num),
                    "-int", str(low_edge), str(high_edge),
                    "-h", os.path.join(REF_PANEL_DIR_PATH, REF_PANEL_BASE_NAME.format(chr_num, "hap")),
                    "-l", os.path.join(REF_PANEL_DIR_PATH, REF_PANEL_BASE_NAME.format(chr_num, "legend")),
                    "-phase",  # for phased output as well
                    "-exclude_snps_g_ref", os.path.join(REF_PANEL_DIR_PATH,
                                                        "{}.snps_bp_to_exclude.txt".format(chr_num)),
                    "-include_snps", os.path.join(REF_PANEL_DIR_PATH,
                                                  "{}.snps_bp_to_include.txt".format(chr_num)),
                    "-o", os.path.join(IMPUTE_DIR_PATH,
                                       IMPUTE_CHUNK_OUTPUT_BASE_NAME.format(chr_num, chunk_num)),
                    ])


def gzip_impute_output_chunks(input_file_names, output_file_name):
    # input_file_paths = [os.path.join(IMPUTE_DIR_PATH, file_name) for file_name in input_file_names]
    # output_file_path = os.path.join(IMPUTE_DIR_PATH, output_file_name)
    # subprocess.run(["tar", "-czf",
    #                 output_file_path + "tar.gz"] + input_file_paths)
    with tarfile.open(os.path.join(IMPUTE_DIR_PATH, output_file_name) + ".tar.gz", "w:gz") as tar_fh:
        for file_name in input_file_names:
            tar_fh.add(os.path.join(IMPUTE_DIR_PATH, file_name), arcname=file_name)


def main(multiprocess=True, remove_temp=True):
    """

    Parameters
    ----------
    multiprocess: bool
        Whether to apply IMPUTE2 in a multi-process fashion. (different chromosomes are still done linearly).
    remove_temp: bool
        Whether to remove intermediate chunk files.

    Returns
    -------

    """
    if not os.path.exists(IMPUTE_DIR_PATH):
        os.makedirs(IMPUTE_DIR_PATH)

    chromosomes = list(range(1, 23))
    for chr_num in chromosomes:
        print("Chromosome {:02} - start processing.".format(chr_num))
        if all([os.path.exists(os.path.join(IMPUTE_DIR_PATH, IMPUTE_OUTPUT_BASE_NAME.format(chr_num))),
                os.path.exists(os.path.join(IMPUTE_DIR_PATH, IMPUTE_OUTPUT_BASE_NAME.format(chr_num)) + "_haps"),
                os.path.exists(os.path.join(IMPUTE_DIR_PATH, IMPUTE_OUTPUT_BASE_NAME.format(chr_num)) + "_info")]):
            # Check for the existence of the most important files:
            print("Chromosome {:02} - skip processing.".format(chr_num))
            continue

        chromosome_map = pd.read_csv(GENETIC_MAP_PATH.format(chr_num), usecols=["position"], sep=" ")
        min_chr_bp = chromosome_map["position"].min()
        max_chr_bp = chromosome_map["position"].max()
        chunks_positions = list(range(min_chr_bp, max_chr_bp + 1, CHUNK_SIZE))
        if chunks_positions[-1] != max_chr_bp:  # if last chromosome position is not included
            chunks_positions += [max_chr_bp]

        print("Chromosome {:02} - apply IMPUTE2 on {} chunks.".format(chr_num, len(chunks_positions)))
        if multiprocess:
            run_impute2_multi_thread(chr_num, chunks_positions)
        else:
            run_impute2_single_thread(chr_num, chunks_positions)
        print("Chromosome {:02} - finished IMPUTE2.".format(chr_num))

        # Concatenate results for entire chromosome:
        # -----------------------------------------
        # get all files associated with current chromosome:
        # chr_output_files = [f for f in os.listdir(IMPUTE_DIR_PATH) if "chr{:02}".format(chr_num) in f]
        chr_output_files = glob.glob(os.path.join(IMPUTE_DIR_PATH,
                                                  "gwas_autosomal.chr{:02}.chunk*.impute2".format(chr_num)))
        # # Sort by chunk num:
        chr_output_files = sorted(chr_output_files, key=lambda x: int(x.split(".")[2].lstrip("chunk")))

        print("Chromosome {:02} - concatenate data.".format(chr_num))
        for extension in ["impute2", "impute2_haps", "_allele_probs"]:
            chr_impute2_ext_files = [f for f in chr_output_files if f.endswith(extension)]
            concat_impute_output_chunks(chr_impute2_ext_files,
                                        IMPUTE_OUTPUT_BASE_NAME.format(chr_num) + extension.lstrip("impute2"),
                                        header="keep")
        # copy the imputed haps file to the haplotype directory:
        shutil.copy2(src=os.path.join(IMPUTE_DIR_PATH, IMPUTE_OUTPUT_BASE_NAME.format(chr_num))+"_haps",
                     dst=os.path.join(GWAS_DIR, "haplotypes", "gwas_autosomal_imputed.chr{:02}.haps".format(chr_num)))

        print("Chromosome {:02} - concatenate info.".format(chr_num))
        for extension in ["_info", "_info_by_sample"]:
            chr_impute2_ext_files = [f for f in chr_output_files if f.endswith(extension)]
            concat_impute_output_chunks(chr_impute2_ext_files, IMPUTE_OUTPUT_BASE_NAME.format(chr_num) + extension,
                                        header="keep_first")

        print("Chromosome {:02} - tar-gzip summaries.".format(chr_num))
        for extension in ["_warnings", "_summary", "_diplotype_ordering"]:
            chr_impute2_ext_files = [f for f in chr_output_files if f.endswith(extension)]
            gzip_impute_output_chunks(chr_impute2_ext_files, IMPUTE_OUTPUT_BASE_NAME.format(chr_num) + extension)

        # Remove chunk files:
        if remove_temp:
            print("Chromosome {:02} - Remove chunk files.".format(chr_num))
            for file_name in chr_output_files:
                if "chunk" in file_name:
                    os.remove(os.path.join(IMPUTE_DIR_PATH, file_name))

    # Concatenate imputed chromosomes into a genome:
    # print("Concatenate chromosome data to genome.")
    # # gen_output_files = [f for f in os.listdir(IMPUTE_DIR_PATH) if (f.startswith("gwas_autosomal.chr") and
    # #                                                                (f.endswith("impute2") or
    # #                                                                 f.endswith("impute2_haps")))]
    # for extension, header_option, engine in zip(["impute2", "impute2_info"], ["keep", "keep_first"],
    #                                             ["python", "python"]):
    #     gen_impute2_ext_files = glob.glob(os.path.join(IMPUTE_DIR_PATH, "gwas_autosomal.chr*.{}".format(extension)))
    #     gen_impute2_ext_files = sorted(gen_impute2_ext_files, key=lambda x: int(x.split(".")[1].lstrip("chr")))
    #     concat_impute_output_chunks(gen_impute2_ext_files,
    #                                 "gwas_autosomal_imputed." + extension,
    #                                 header=header_option, to_gzip=True, engine=engine)
    # print("Convert merged imputed files into PLINK format")


if __name__ == '__main__':
    main()
