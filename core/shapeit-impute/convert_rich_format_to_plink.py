import os
import subprocess

import pandas as pd
from scipy.interpolate import interp1d


if os.name == "nt":
    DATA_DIR = r"C:\Studies\MSc\carmi_thesis\research\data"
    PLINK_EXE_PATH = r"C:\Studies\MSc\carmi_thesis\research\toolsets\plink_v1.90\plink.exe"
elif os.name == "posix":
    DATA_DIR = os.path.join(os.sep, "cs", "icore", "ehudk", "carmi_thesis", "data")
    PLINK_EXE_PATH = os.path.join(os.sep, "cs", "usr", "ehudk", "safe", "carmi_thesis", "toolsets",
                                  "plink_v1.90", "plink")
else:
    raise EnvironmentError

# INPUT_FILE_NAME = "gwas_autosomal_imputed.chr{:02}.pkl"
# INPUT_FILE_NAME = "CENT-SO_2018-07-17_19-15-41_{}.pkl"
# INPUT_FILE_NAME = "CENT-SO_2018-07-19_20-00-06_{}.pkl"
# INPUT_FILE_NAME = "CENT-SO_2018-08-23_09-49-27_{}.pkl"
# INPUT_FILE_NAME = "CENT-SO_2018-11-17_10-00-02_{}.pkl"
INPUT_FILE_NAME = "CENT-SO_2018-11-17_10-14-15_{}.pkl"
# OUTPUT_BASE_NAME = "gwas_autosomal_imputed.chr{:02}.{}"
# OUTPUT_BASE_NAME = "CENT-SO_2018-07-17_19-15-41_{}.{}"
# OUTPUT_BASE_NAME = "CENT-SO_2018-07-19_20-00-06_{}.{}"
# OUTPUT_BASE_NAME = "CENT-SO_2018-08-23_09-49-27_{}.{}"
# OUTPUT_BASE_NAME = "CENT-SO_2018-11-17_10-00-02_{}.{}"
OUTPUT_BASE_NAME = "CENT-SO_2018-11-17_10-14-15_{}.{}"
# INPUT_DIR_PATH = os.path.join(DATA_DIR, "cent_kids", "haplotypes")
INPUT_DIR_PATH = os.path.join(DATA_DIR, "cent_kids", "offspring_simulations")
OUTPUT_DIR_PATH = INPUT_DIR_PATH
INPUT_FILE_PATH = os.path.join(INPUT_DIR_PATH, INPUT_FILE_NAME)
OUTPUT_BASE_PATH = os.path.join(OUTPUT_DIR_PATH, OUTPUT_BASE_NAME)

GENETIC_MAP_BASE_PATH = os.path.join(DATA_DIR, "genetic_map_b37", "genetic_map_chr{}_combined_b37.txt")
FAM_FILE_PATH = os.path.join(DATA_DIR, "cent_kids", "gwas_autosomal.fam")   # constant to all chromosomes.


def convert_rich_to_tped(chr_num):
    rich_haps = pd.read_pickle(INPUT_FILE_PATH.format(chr_num))
    gmap = pd.read_csv(GENETIC_MAP_BASE_PATH.format(chr_num), sep=" ")

    # reorder the known fam file by the order of the current haplotypes/genotypes:
    fam_table = pd.read_csv(FAM_FILE_PATH, sep=" ", header=None,
                            names=["FID", "IID", "Father", "Mother", "Sex", "Pheno"])
    fam_current = rich_haps.columns.to_frame()[["FID", "IID"]]
    fam_current = fam_current.drop_duplicates(subset=["FID", "IID"])  # Each appears twice - one for each haplotype
    fam_table = fam_table.set_index(["FID", "IID"])
    fam_current = fam_current.set_index(["FID", "IID"])
    assert set(fam_table.index) == set(fam_current.index)
    fam_table = fam_table.loc[fam_current.index]
    fam_table = fam_table.reset_index()
    fam_table = fam_table[["FID", "IID", "Father", "Mother", "Sex", "Pheno"]]

    # map:
    f = interp1d(gmap["position"].values, gmap["Genetic_Map(cM)"].values, "slinear", fill_value="extrapolate")
    map_table = rich_haps.index.to_frame()
    interp_cm = f(map_table["bp"].values)
    interp_cm[interp_cm <= 0] = 1e-6  # avoid negative values. but 0 means missing, so go small.
    map_table["cm"] = interp_cm
    map_table = map_table[["chr", "snp", "cm", "bp"]]   # rearrange column order.
    map_table = map_table.drop_duplicates(subset=["snp", "bp"], keep="first")

    rich_haps.columns = range(rich_haps.shape[1])
    rich_haps = rich_haps.reset_index()
    rich_haps = rich_haps.drop_duplicates(subset=["snp", "bp"], keep="first")
    assert set(rich_haps["snp"]) == set(map_table["snp"])   # all(rich_haps["snp"].values == map_table["snp"].values)
    assert set(rich_haps["bp"]) == set(map_table["bp"])
    tped = pd.merge(map_table, rich_haps, on=["chr", "snp", "bp"], how="left")
    if (tped.shape[0] != rich_haps.shape[0]) or (tped.shape[0] != map_table.shape[0]):
        tped = tped.drop_duplicates()
    assert tped.shape[0] == rich_haps.shape[0]
    assert tped.shape[0] == map_table.shape[0]

    fam_table.to_csv(OUTPUT_BASE_PATH.format(chr_num, "tfam"), sep=" ", header=False, index=False)
    map_table.to_csv(OUTPUT_BASE_PATH.format(chr_num, "map"), sep=" ", header=False, index=False)
    tped.to_csv(OUTPUT_BASE_PATH.format(chr_num, "tped"), sep=" ", header=False, index=False)


def convert_tped_to_bed(chr_num):
    subprocess.run(args=[PLINK_EXE_PATH,
                         "--nonfounders",
                         "--allow-no-sex",
                         "--tfile", OUTPUT_BASE_PATH.format(chr_num, "")[:-1],  # discard the extension placeholder
                         "--make-bed",
                         "--out", OUTPUT_BASE_PATH.format(chr_num, "")[:-1]     # discard the extension placeholder
                         ])

    output_files_valid = [os.path.exists(OUTPUT_BASE_PATH.format(chr_num, extension)) and       # Exists
                          os.path.getsize(OUTPUT_BASE_PATH.format(chr_num, extension)) > 0      # Not degenerate
                          for extension in ["bed", "bim", "fam"]]
    if all(output_files_valid):
        os.remove(OUTPUT_BASE_PATH.format(chr_num, "tped"))
        os.remove(OUTPUT_BASE_PATH.format(chr_num, "tfam"))
        os.remove(OUTPUT_BASE_PATH.format(chr_num, "map"))


def process_one_chromosome(chr_num):
    if not all([os.path.exists(OUTPUT_BASE_PATH.format(chr_num, "bed")),
                os.path.exists(OUTPUT_BASE_PATH.format(chr_num, "bim")),
                os.path.exists(OUTPUT_BASE_PATH.format(chr_num, "fam"))]):

        if not all([os.path.exists(OUTPUT_BASE_PATH.format(chr_num, "tped")),
                    os.path.exists(OUTPUT_BASE_PATH.format(chr_num, "map")),
                    os.path.exists(OUTPUT_BASE_PATH.format(chr_num, "tfam"))]):
            # Check for the existence of the most important files:
            print("Chromosome {:02} - Convert to tped.".format(chr_num))
            convert_rich_to_tped(chr_num)

        print("Chromosome {:02} - Convert to bed.".format(chr_num))
        convert_tped_to_bed(chr_num)


def merge_bed_chromosomes(chromosomes, output_base_path=None):
    output_base_path = OUTPUT_BASE_PATH.rsplit("_", 1)[0] if output_base_path is None else output_base_path
    print("Gonna merge all chromosomal bed to one")
    files_prefixes_to_merge = [OUTPUT_BASE_PATH.format(chr_num, "")[:-1]
                               for chr_num in chromosomes]  # chr01 will be reference
    with open(os.path.join(OUTPUT_DIR_PATH, "plink_list_for_merge"), "w") as output_fh:
        output_fh.write("\n".join(files_prefixes_to_merge))
    subprocess.run([PLINK_EXE_PATH,
                    "--bfile", OUTPUT_BASE_PATH.format(1, "")[:-1],
                    "--merge-list", os.path.join(OUTPUT_DIR_PATH, "plink_list_for_merge"),
                    "--make-bed",
                    # "--out", os.path.join(OUTPUT_DIR_PATH, "..", "gwas_autosomal_imputed")
                    "--out", output_base_path
                    ])


def main():
    if not os.path.exists(OUTPUT_DIR_PATH):
        os.makedirs(OUTPUT_DIR_PATH)

    # chromosomes = list(range(1, 23))
    # chromosomes = list(range(10))
    chromosomes = [0, 1]
    # chromosomes = [22]
    for chr_num in chromosomes:
        print("Chromosome {:02} - start processing.".format(chr_num))
        process_one_chromosome(chr_num)

    merge_bed_chromosomes(chromosomes)


def convert_tped_to_rich_format(plink_base_path, to_save=False):
    converted_to_tped = False
    if not os.path.exists(plink_base_path + ".tped") and os.path.exists(plink_base_path + ".bed"):
        # Convert bed to tped
        subprocess.run(args=[PLINK_EXE_PATH,
                             "--nonfounders",
                             "--allow-no-sex",
                             "--bfile", plink_base_path,  # discard the extension placeholder
                             "--recode", "transpose",
                             "--out", plink_base_path     # discard the extension placeholder
                             ])
        if os.path.exists(plink_base_path + ".tped"):
            converted_to_tped = True
        else:
            raise FileExistsError("Was not able to create tped file from provided bed file")

    tped = pd.read_csv(plink_base_path + ".tped", sep=" ", header=None)
    mapp, tped = tped.iloc[:, :4], tped.iloc[:, 4:]                         # type: pd.DataFrame, pd.DataFrame
    fam = pd.read_csv(plink_base_path + ".tfam", sep=" ", header=None)

    fam = fam[[0, 1]].rename(columns={0: "FID", 1: "IID"})
    fam = fam.iloc[pd.np.repeat(pd.np.arange(fam.shape[0]), 2)]     # repeat participants (one for each haplotype)
    fam["HAP"] = pd.np.tile([0, 1], fam.shape[0] // 2)
    mapp = mapp.rename(columns={0: "chr", 1: "snp", 2: "cm", 3: "bp"}).drop("cm", axis="columns")

    assert fam.shape[0] == tped.shape[1]
    assert mapp.shape[0] == tped.shape[0]

    cols = fam.set_index(["FID", "IID", "HAP"]).index
    index = mapp.set_index(["chr", "snp", "bp"]).index

    tped.columns = cols
    tped.index = index

    if converted_to_tped:
        # Clean up:
        os.remove(plink_base_path + ".tped")
        os.remove(plink_base_path + ".tfam")
        if os.path.exists(plink_base_path + ".nosex"):
            os.remove(plink_base_path + ".nosex")

    if to_save:
        tped.to_pickle(plink_base_path + ".pkl")

    return tped


if __name__ == '__main__':
    main()

