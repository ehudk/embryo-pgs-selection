import multiprocessing
import subprocess
import os

if os.name == "nt":
    DATA_DIR = r"C:\Studies\MSc\carmi_thesis\research\data"
    GENETIC_MAP_PATH = os.path.join(DATA_DIR, "genetic_map_b37")
    GWAS_DIR = os.path.join(DATA_DIR, "cent_kids")
    PLINK_EXE_PATH = r"C:\Studies\MSc\carmi_thesis\research\toolsets\plink_v1.90\plink.exe"
    SHAPEIT_EXE_PATH = ""
elif os.name == "posix":
    DATA_DIR = os.path.join(os.sep, "cs", "icore", "ehudk", "carmi_thesis", "data")
    GENETIC_MAP_PATH = os.path.join(DATA_DIR, "genetic_map_b37")
    GWAS_DIR = os.path.join(DATA_DIR, "cent_kids")
    SHAPEIT_EXE_PATH = os.path.join(os.sep, "cs", "usr", "ehudk", "safe", "carmi_thesis", "toolsets",
                                    "shapeit", "shapeit.v2.904.3.10.0-693.11.6.el7.x86_64", "bin",
                                    "shapeit")
    PLINK_EXE_PATH = os.path.join(os.sep, "cs", "usr", "ehudk", "safe", "carmi_thesis", "toolsets",
                                  "plink_v1.90", "plink")
    # PLINK_EXE_PATH = "plink1"
else:
    raise EnvironmentError
BASE_NAME = "gwas_autosomal"
HAPLOTYPE_DIR_NAME = "haplotypes"


def main():
    # Create output directory if not present:
    output_dir_path = os.path.join(GWAS_DIR, HAPLOTYPE_DIR_NAME)
    if not os.path.exists(output_dir_path):
        os.makedirs(output_dir_path)
    print("Number of CPUs:", multiprocessing.cpu_count())

    pool = multiprocessing.Pool()
    arguments = zip(range(1, 23), [output_dir_path] * 22)
    pool.starmap(process_single_chr, arguments)


def process_single_chr(i, output_dir_path):
    print("Chromosome {}".format(i))
    output_file_base_name = os.path.join(output_dir_path, BASE_NAME + ".chr{:02}".format(i))

    # Create split PLINK files if necessary:
    if not all([os.path.exists(output_file_base_name + ext) for ext in {".bed", ".bim", ".fam"}]):
    # if not all([os.path.exists(output_file_base_name + ext) for ext in {".ped", ".map"}]):
        print("Splitting PLINK genotypes by chromosome")
        subprocess.run(args=[PLINK_EXE_PATH,
                             # "--nonfounders",
                             "--allow-no-sex",
                             "--bfile", os.path.join(GWAS_DIR, BASE_NAME),
                             # "--file", os.path.join(GWAS_DIR, BASE_NAME),
                             "--chr", str(i),
                             "--make-bed",
                             # "--recode",
                             "--out", output_file_base_name])

    # Run shape it to graph output:
    if not os.path.exists(output_file_base_name + ".graph"):
        print("Running SHAPEIT")
        subprocess.run(args=[SHAPEIT_EXE_PATH,
                             "-B", output_file_base_name,
                             # "-P", output_file_base_name,
                             "-M", os.path.join(GENETIC_MAP_PATH, "genetic_map_chr{}_combined_b37.txt".format(i)),
                             "--output-graph", output_file_base_name + ".graph",
                             "--output-log", output_file_base_name + ".shapeit.log",
                             "--states", "200",
                             "--effective-size", "14000",
                             # "-T", 4
                             ])

    # Convert graph output to deterministic output:
    if not all([os.path.exists(output_file_base_name + ext) for ext in {".haps", ".sample"}]):
        print("Converting SHAPEIT graph output to deterministic one")
        subprocess.run(args=[SHAPEIT_EXE_PATH,
                             "-convert",
                             "--input-graph", output_file_base_name + ".graph",
                             "--output-max", output_file_base_name + ".haps", output_file_base_name + ".sample"])


if __name__ == '__main__':
    main()
