import os
import shutil
import pandas as pd

if os.name == "nt":
    DATA_DIR = r"C:\Studies\MSc\carmi_thesis\research\data"
    PLINK_EXE_PATH = r"C:\Studies\MSc\carmi_thesis\research\toolsets\plink_v1.90\plink.exe"
elif os.name == "posix":
    DATA_DIR = os.path.join(os.sep, "cs", "icore", "ehudk", "carmi_thesis", "data")
    PLINK_EXE_PATH = os.path.join(os.sep, "cs", "usr", "ehudk", "safe", "carmi_thesis", "toolsets",
                                  "plink_v1.90", "plink")
else:
    raise EnvironmentError

INPUT_DIR_PATH = os.path.join(DATA_DIR, "cent_kids", "impute")
OUTPUT_DIR_PATH = INPUT_DIR_PATH
INPUT_BASE_PATH = os.path.join(INPUT_DIR_PATH, "gwas_autosomal.chr{:02}.{}")
OUTPUT_BASE_PATH = os.path.join(OUTPUT_DIR_PATH, "gwas_autosomal_filt{}snps.chr{:02}.{}")
INCLUDE_SNPS_FULL_INFO_BASE_PATH = os.path.join(DATA_DIR, "tagc_reference_panel", "{}.snps_info_to_include.txt")


def preprocess_info_file(chr_num):
    if os.path.exists(INPUT_BASE_PATH.format(chr_num, "impute2_info_clean")):
        print("Chromosome {:02}: clean info-file is present".format(chr_num))
        return

    print("Chromosome {:02}: create new and clean info-file".format(chr_num))
    with open(INPUT_BASE_PATH.format(chr_num, "impute2_info"), "r") as input_fh, \
            open(INPUT_BASE_PATH.format(chr_num, "impute2_info_clean"), "w") as output_fh:
        output_fh.write(next(input_fh))  # write first header
        for line in input_fh:
            if line.startswith("snp"):
                continue
            output_fh.write(line)
    # os.remove(INPUT_BASE_PATH.format(chr_num, "impute2_info"))
    # shutil.move(src=INPUT_BASE_PATH.format(chr_num, "impute2_info_clean"),
    #             dst=INPUT_BASE_PATH.format(chr_num, "impute2_info"))


def filter_by_snp_and_info(chr_num, info_score_cutoff):

    include_snps = pd.read_csv(INCLUDE_SNPS_FULL_INFO_BASE_PATH.format(chr_num))
    include_snps_bp = set(include_snps["pos"])
    print("Chromosome {:02}: {} SNPs. read include SNPs".format(chr_num, len(include_snps_bp)))

    # Filter info file based on snps:
    info = pd.read_csv(INPUT_BASE_PATH.format(chr_num, "impute2_info_clean"), sep=" ")

    print("Chromosome {:02}: {} SNPs. read info file".format(chr_num, info.shape[0]))
    info = info.loc[info["position"].isin(include_snps_bp)]
    print("Chromosome {:02}: {} SNPs. filter based on SNP list".format(chr_num, info.shape[0]))

    # Filter info file based on info score
    info = info.loc[info["info"] > info_score_cutoff]
    print("Chromosome {:02}: {} SNPs. filter based on info score ({})".format(chr_num, info.shape[0], info_score_cutoff))
    info = info.loc[(info["a0"].str.len() == 1) & (info["a1"].str.len() == 1)]
    print("Chromosome {:02}: {} SNPs. filter on multi-nuc alleles".format(chr_num, info.shape[0]))
    info = info.dropna(subset=["position"])
    info["position"] = info["position"].astype(int)

    info = pd.merge(info, include_snps[["pos", "name"]], left_on="position", right_on="pos")
    info["snp_id"] = info["name"]
    info["rs_id"] = info["name"]
    info = info.drop(["pos", "name"], axis="columns")
    print("Chromosome {:02}: {} SNPs. fill snp names".format(chr_num, info.shape[0]))

    # Drop duplicates by keeping the ones with highest type and highest info
    info = info.sort_values(["type", "info"]).drop_duplicates(subset="position", keep="last").sort_index()
    print("Chromosome {:02}: {} SNPs. drop duplicate positions".format(chr_num, info.shape[0]))

    include_snps_bp = set(info["position"])
    info = info.set_index("position")

    # Filter the rest of the files:
    for extension in ["impute2", "impute2_haps"]:     # Can add allele_probs if present
        print("Chromosome {:02}: filtering {} file".format(chr_num, extension))
        with open(INPUT_BASE_PATH.format(chr_num, extension), "r") as input_fh, \
                open(OUTPUT_BASE_PATH.format(info_score_cutoff, chr_num, extension), "w") as output_fh:
        # with open(INPUT_BASE_PATH.format(chr_num, extension), "r") as input_fh:
            for input_line in input_fh:
                _, _, position, line_data = input_line.split(" ", 3)
                if not position.isnumeric():
                    print("Got non numeric position value: {}".format(position))
                    continue
                position = int(position)
                if position in include_snps_bp:
                    snp_header = " ".join([str(chr_num), info.loc[position, "rs_id"], str(position)])
                    output_fh.write(snp_header + " " + line_data)

    # Save new info file:
    info = info.reset_index()
    print("Chromosome {:02}: filtering info file".format(chr_num))
    info.to_csv(OUTPUT_BASE_PATH.format(info_score_cutoff, chr_num, "impute_info"), sep=" ", index=False)


def main():
    chromosomes = list(range(1, 23))[::-1]
    info_score_sutoff = 0.9
    for chr_num in chromosomes:
        if not all([os.path.exists(OUTPUT_BASE_PATH.format(info_score_sutoff, chr_num, extension))
                    for extension in {"impute", "impute_haps", "impute_info"}]):
            preprocess_info_file(chr_num)
            filter_by_snp_and_info(chr_num, info_score_sutoff)

        # if all([OUTPUT_BASE_PATH.format(FILTER_NUM_INFO_SCORE, chr_num, extension)
        #         for extension in {"impute", "impute_haps", "impute_info"}]):
        #     continue
        # filter_by_info_score(chr_num)


if __name__ == '__main__':
    main()





