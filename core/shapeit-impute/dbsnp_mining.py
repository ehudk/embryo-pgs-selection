import requests
import pandas as pd
import time

DBSNP_BASE_URL = r"https://api.ncbi.nlm.nih.gov/variation/v0/"
DBSNP_REQ = DBSNP_BASE_URL + r"beta/refsnp/{rsid}"


def mine_snps(variants, sleep_time=1):
    snp_results = []
    for snp_id in variants:
        if snp_id.startswith("rs"):
            snp_id_num = int(snp_id[2:])
            snp_id_num, new_snp_id_num, chr_num, position = get_snp_data(snp_id_num)
            snp_id, new_snp_id = "rs{}".format(snp_id_num), "rs{}".format(new_snp_id_num)
        else:
            new_snp_id, chr_num, position = None, None, None
        snp_results.append([snp_id, new_snp_id, chr_num, position])
        time.sleep(sleep_time)
    snp_results_df = pd.DataFrame.from_records(snp_results,
                                               columns=["old_snp_id", "new_snp_id", "chr", "bp"])
    return snp_results_df


def get_snp_data(snp_id_num):
    response = requests.get(DBSNP_REQ.format(rsid=snp_id_num))
    data = response.json()
    if 'withdrawn_snapshot_data' in data:
        position = 0
        chr_num = 1  # this is how they all showed
        new_snp_id_num = snp_id_num
    elif 'merged_snapshot_data' in data:
        new_snp_id_num = int(data['merged_snapshot_data']['merged_into'][0])
        _, new_snp_id_num, chr_num, position = get_snp_data(new_snp_id_num)
    elif 'present_obs_movements' in data:
        snp_data = data['present_obs_movements'][0]['allele_in_cur_release']
        position = snp_data['position']
        chr_num = int(snp_data['seq_id'].split('_')[1].split('.')[0])
        new_snp_id_num = snp_id_num
    else:
        print("Wasn't any one of the options: {}".format(snp_id_num))
        new_snp_id_num, chr_num, position = None, None, None
    return snp_id_num, new_snp_id_num, chr_num, position


test_list = ["rs10250922", "rs1050211", "rs1059667", "rs1073731", "rs10807836"]
results = mine_snps(test_list)
print("End")
