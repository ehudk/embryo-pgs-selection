import pandas as pd
import dask.dataframe as dd
import os
import subprocess
import glob


if os.name == "nt":
    DATA_DIR = r"C:\Studies\MSc\carmi_thesis\research\data"
    PLINK_EXE_PATH = r"C:\Studies\MSc\carmi_thesis\research\toolsets\plink_v1.90\plink.exe"
elif os.name == "posix":
    DATA_DIR = os.path.join(os.sep, "cs", "icore", "ehudk", "carmi_thesis", "data")
    PLINK_EXE_PATH = os.path.join(os.sep, "cs", "usr", "ehudk", "safe", "carmi_thesis", "toolsets",
                                  "plink_v1.90", "plink")
else:
    raise EnvironmentError

# File names:
SAMPLE_BASE_NAME = "gwas_autosomal.chr{:02}.sample"
IMPUTE_OUTPUT_BASE_NAME = "gwas_autosomal.chr{:02}.impute2"
PLINK_CHR_OUTPUT = "gwas_autosomal_imputed.chr{:02}"
PLINK_GEN_OUTPUT = "gwas_autosomal_imputed"

# Files location:
GWAS_DIR = os.path.join(DATA_DIR, "cent_kids")
HAPLOTYPE_DIR_PATH = os.path.join(GWAS_DIR, "haplotypes")
IMPUTE_DIR_PATH = os.path.join(GWAS_DIR, "impute")


dbsnp = dd.read_csv(os.path.join(DATA_DIR, "dbsnp", "dbsnp147_uniq_3col.txt"),
                    sep=" ", header=None, names=["chr", "bp", "name"],
                    dtype={"chr": str, "bp": int, "name": str})
sumstats_height = pd.read_csv(os.path.join(DATA_DIR,
                                           "giant_height_2018", "Meta-analysis_Wood_et_al+UKBiobank_2018.txt"),
                              sep="\t")
sumstats_cog = pd.read_csv(os.path.join(DATA_DIR, "cog_ablt", "iq.edutrait_mtag.assoc.gz"), sep="\t")


for chr_num in range(1, 23):
    print("Chromosome {:02}".format(chr_num))
    if all([os.path.exists(os.path.join(IMPUTE_DIR_PATH, PLINK_CHR_OUTPUT.format(chr_num) + extension))
            for extension in [".bed", ".bim", ".fam"]]):
        print("Chromosome {:02} - skipping".format(chr_num))
        continue

    # Convert IMPUTE2 to PLINK:
    print("Chromosome {:02} - converting with PLINK".format(chr_num))
    subprocess.run([PLINK_EXE_PATH,
                    "--gen", os.path.join(IMPUTE_DIR_PATH, IMPUTE_OUTPUT_BASE_NAME.format(chr_num)),
                    "--sample", os.path.join(HAPLOTYPE_DIR_PATH, SAMPLE_BASE_NAME.format(chr_num)),
                    "--oxford-single-chr", str(chr_num),
                    "--hard-call-threshold", "0.1",
                    "--geno", "0.1",
                    "--make-bed",
                    "--out", os.path.join(IMPUTE_DIR_PATH, PLINK_CHR_OUTPUT.format(chr_num))])

    print("Chromosome {:02} - editing bim file".format(chr_num))
    # Edit bim file (add SNP name [we don't care for cM, cause we ain't gonna simulate from that)
    bim_table = pd.read_csv(os.path.join(IMPUTE_DIR_PATH, PLINK_CHR_OUTPUT.format(chr_num) + ".bim"), sep="\t",
                            header=None, names=["chr", "name", "cm", "bp", "a1", "a2"])
    joined = dd.merge(dbsnp.loc[dbsnp["chr"] == str(chr_num)], bim_table,
                      how="right", left_on="bp", right_on="bp").compute()
    # Fill missing snp-ids:
    mutual_non_missing_mask = (joined["name_x"].isnull()) & (joined["name_y"] != ".")
    joined.loc[mutual_non_missing_mask, "name_x"] = joined.loc[mutual_non_missing_mask, "name_y"]
    # # Try using the sum-stats for filling as well.
    joined2 = pd.merge(joined, sumstats_height.loc[sumstats_height["CHR"] == chr_num, ["CHR", "POS", "SNP"]],
                       how="left", left_on="bp", right_on="POS")
    mutual_non_missing_mask = (joined2["name_x"].isnull()) & (joined2["SNP"].notnull())
    joined2.loc[mutual_non_missing_mask, "name_x"] = joined2.loc[mutual_non_missing_mask, "SNP"]

    joined3 = pd.merge(joined2, sumstats_cog.loc[sumstats_cog["CHR"] == chr_num, ["CHR", "BP", "SNP"]],
                       how="left", left_on="bp", right_on="BP")
    mutual_non_missing_mask = (joined3["name_x"].isnull()) & (joined3["SNP_y"].notnull())
    joined3.loc[mutual_non_missing_mask, "name_x"] = joined3.loc[mutual_non_missing_mask, "SNP_y"]
    # re-name the missing name, so SNP_names would be unique:
    joined3.loc[joined3["name_x"].isnull(), "name_x"] = ["{}_missing_{}".format(chr_num, i)
                                                         for i in range(joined3["name_x"].isnull().sum())]

    joined3["chr_y"] = chr_num
    # deal with duplicated variants:
    dups_joined3 = joined3.loc[joined3["name_x"].duplicated(keep=False)]
    dups_joined3.loc[(dups_joined3["a1"] == "0") | (dups_joined3["a2"] == "0"), "name_x"] += "_edited_out"
    dups_joined3.loc[(dups_joined3["a1"] == "0") | (dups_joined3["a2"] == "0"), "bp"] += 1
    dups_joined3.loc[(dups_joined3["a1"].str.len() > 1) | (dups_joined3["a2"].str.len() > 1), "name_x"] += "_edited_out"
    dups_joined3.loc[(dups_joined3["a1"].str.len() > 1) | (dups_joined3["a2"].str.len() > 1), "bp"] += 1
    if not dups_joined3.loc[dups_joined3.duplicated(keep=False)].empty:
        print("THERE ARE STILL DUPS")
    joined3.loc[dups_joined3.index] = dups_joined3
    # FIXME: does not solve all cases

    # re-write the adjusted bim file:
    bim_table = joined3[["chr_y", "name_x", "cm", "bp", "a1", "a2"]]
    bim_table.to_csv(os.path.join(IMPUTE_DIR_PATH, PLINK_CHR_OUTPUT.format(chr_num) + ".bim"),
                     sep="\t", index=False, header=False)

# merge chromosomes together:
print("Merging all PLINK chromosomes together")
files_prefixes_to_merge = [os.path.join(IMPUTE_DIR_PATH, PLINK_CHR_OUTPUT.format(chr_num))
                           for chr_num in range(2, 23)]    # chr01 will be reference
with open(os.path.join(IMPUTE_DIR_PATH, "plink_list_for_merge"), "w") as output_fh:
    output_fh.write("\n".join(files_prefixes_to_merge))
subprocess.run([PLINK_EXE_PATH,
                "--bfile", os.path.join(IMPUTE_DIR_PATH, PLINK_CHR_OUTPUT.format(1)),
                "--merge-list", os.path.join(IMPUTE_DIR_PATH, "plink_list_for_merge"),
                "--make-bed",
                "--out", os.path.join(IMPUTE_DIR_PATH, PLINK_GEN_OUTPUT)
                ])
