import pandas as pd
import numpy as np
import os
import multiprocessing
# import time


# SHAPEIT_BASE_FILE_NAME = "kib.chr.{}.{}"
# SHAPEIT_BASE_FILE_NAME = "aspis_b37_qc_eur_norel.chr{}.{}"
# SHAPEIT_BASE_FILE_NAME = "gwas_autosomal.chr{:02}.{}"
SHAPEIT_BASE_FILE_NAME = "gwas_autosomal_imputed.chr{:02}.{}"
if os.name == "nt":
    # SHAPEIT_DIR = os.path.join("..", "data", "kibbutzim", "haplotypes_ped")
    # SHAPEIT_DIR = os.path.join("..", "data", "cog_ablt", "haplotypes_ped")
    SHAPEIT_DIR = os.path.join("..", "data", "cent_kids", "haplotypes")
    OUTPUT_DIR = SHAPEIT_DIR
elif os.name == "posix":
    # SHAPEIT_DIR = os.path.join(os.path.sep, "cs", "icore", "ehudk", "carmi_thesis", "data", "kibbutzim",
    #                            "haplotypes_ped")
    # SHAPEIT_DIR = os.path.join(os.path.sep, "cs", "icore", "ehudk", "carmi_thesis", "data", "cog_ablt",
    #                            "haplotypes_ped")
    SHAPEIT_DIR = os.path.join(os.path.sep, "cs", "icore", "ehudk", "carmi_thesis", "data", "cent_kids", "haplotypes")
    OUTPUT_DIR = SHAPEIT_DIR
else:
    raise EnvironmentError


def load_shapeit_files(haps_file_path, sample_file_path):
    haps = pd.read_csv(haps_file_path,
                       delim_whitespace=True, header=None, index_col=list(range(5)))
    sample = pd.read_csv(sample_file_path,
                         delim_whitespace=True, header=0, skiprows=[1], usecols=[0, 1], names=["FID", "IID"])
    return haps, sample


def convert_shapeit_to_rich_format(haps, sample):
    """

    Parameters
    ----------
    haps : pd.DataFrame
        haps file loaded with load_shapeit_files
    sample : pd.DataFrame
        sample file loaded with load_shapeit_files

    Returns
    -------
        pd.DataFrame: rich-format haplotype file.
    """

    # NOTE: depends on HAPS and SAMPLE files having corresponding order of haplotypes/variant.
    # sample has half the entries, because the individual appears in it once, but twice in haps (two haplotypes):
    sample = sample.loc[sample.index.repeat(2)]
    # Now that we've "doubled" the number of individual, each can be assigned a haplotype (0 or 1):
    sample.loc[:, "HAP"] = np.tile(np.array([0, 1]), (sample.index.size // 2, ))

    # Rename haps columns to correspond to family-id & individual-id & haplotype:
    haps.columns = sample.set_index(list(sample.columns)).index

    # Rename haps index to have meaningful names:  (allele 1 and allele 2 to turn 0 and 1 for later look up)
    haps.index = haps.index.set_names(["chr", "snp", "bp", 0, 1])

    # Export the alleles:
    alleles = pd.concat([haps.index.get_level_values(0).to_frame().reset_index(drop=True),
                         haps.index.get_level_values(1).to_frame().reset_index(drop=True)],
                        axis="columns")
    # Remove alleles from haps:
    haps.index = haps.index.droplevel(0)
    haps.index = haps.index.droplevel(1)

    # Change alleles index to speak same language as maps:
    alleles.index = haps.index

    # Some elaborate lookup table:
    # Go over every row, and for each row map each element (numerical haplotype) to an element based on the same row in
    # alleles (i.e. the variant name) and the column in alleles (that is the numerical haplotype coding for this snp).
    # haps = haps.apply(lambda x: x.map(lambda y: alleles.loc[x.name, y]), axis="columns")

    # res_haps = pd.DataFrame(index=haps.index, columns=haps.columns, dtype=object)
    # for col_idx, col in haps.iteritems():
    #     for row_idx, cell in col.iteritems():
    #         res_haps.at[row_idx, col_idx] = alleles.at[row_idx, cell]

    res_haps = pd.DataFrame(index=haps.index, columns=haps.columns, dtype=object)
    for row_num, (row_idx, row_data) in enumerate(haps.iterrows()):
        # for each row create a dictionary out off alleles and apply that mapping.

        # res_haps.at[row_idx, :] = haps.loc[row_idx].applymap(alleles.loc[row_idx].iloc[0].get)    # For imputed
        # res_haps.at[row_idx, :] = row.map(alleles.loc[row_idx].get)                               # For un-imputed
        res_haps.iloc[row_num] = row_data.map(alleles.iloc[row_num].get)                            # For imputed
        # # alleles.iloc[0].name == row_idx

        # if "imputed" in SHAPEIT_BASE_FILE_NAME.lower():
        #     # This is just a stupid technical issue since IMPUTE2 output has no chromosome and SNP-name:
        #     alleles_map = alleles.loc[row_idx].iloc[0]
        #     res_haps.at[row_idx, :] = row.map(alleles_map.get).to_frame().T
        # else:
        #     # This is the original assignment
        #     res_haps.at[row_idx, :] = row.map(alleles.loc[row_idx].get)

    return res_haps


def process_all_shapeit_files(shapeit_dir=SHAPEIT_DIR, shapeit_base_file_name=SHAPEIT_BASE_FILE_NAME,
                              output_dir=OUTPUT_DIR, chromosomes=22):
    """
    Parameters
    ----------
    shapeit_dir : str
        path to directory containing the SHAPEIT output.
    shapeit_base_file_name : str
        string with placeholders for chromosome number and file extension.
    output_dir : str
        directory to output the rich-format haplotypes files.
    chromosomes : int or list[int]
        if int - the number of chromosomes to process. If list - the list of chromosomes to process (probably one-based
        count.

    Returns
    -------
        pd.DataFrame: Rich format of haplotypes.
    """
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    if isinstance(chromosomes, int):
        chromosomes = list(range(1, chromosomes + 1))[::-1]

    # multiprocess version:
    # n_processes = min(multiprocessing.cpu_count() // 2, len(chromosomes))
    # print(n_processes)
    # pool = multiprocessing.Pool(processes=n_processes)
    pool = multiprocessing.Pool()
    arguments = zip(chromosomes,
                    [shapeit_base_file_name] * len(chromosomes),
                    [shapeit_dir] * len(chromosomes),
                    [output_dir] * len(chromosomes))
    pool.starmap(process_one_chromosome, arguments)
    pool.close()
    pool.join()
    # for chr_num in chromosomes:
    #     process_one_chromosome(chr_num, shapeit_base_file_name, shapeit_dir, output_dir)


def process_one_chromosome(chr_num, shapeit_base_file_name, input_dir, output_dir, to_return=False):
    print("Processing chromosome {}".format(chr_num))
    haps_file_path = os.path.join(input_dir, shapeit_base_file_name.format(chr_num, "haps"))
    sample_file_path = os.path.join(input_dir, shapeit_base_file_name.format(chr_num, "sample"))
    output_serialize_path = os.path.join(output_dir, shapeit_base_file_name.format(chr_num, "pkl"))
    output_text_path = os.path.join(output_dir, shapeit_base_file_name.format(chr_num, "csv"))
    # output_hdf5_path = os.path.join(output_dir, shapeit_base_file_name.format(chr_num, "hdf"))

    # Skip if exiting:
    if os.path.exists(output_text_path) and os.path.exists(output_serialize_path):
        print("\tchromosome #{} already processed. Skipping".format(chr_num))
        if to_return:
            return pd.read_csv(output_text_path)
        else:
            return

    print("Loading SHAPEIT files")
    haps, sample = load_shapeit_files(haps_file_path, sample_file_path)
    print("Converting SHAPEIT files to one rich format file")
    print("-- this might take a while...")
    rich_haps = convert_shapeit_to_rich_format(haps, sample)
    print("Saving output serializable (pickle)")
    rich_haps.to_pickle(output_serialize_path)
    # print("Saving output textually (csv)")
    # rich_haps.to_csv(output_text_path)
    if to_return:
        return rich_haps
    else:
        return


def tryouttest():
    h, s = load_shapeit_files(r"C:\Studies\MSc\carmi_thesis\research\data\kibbutzim\haplotypes_ped\kib.chr.22.haps",
                              r"C:\Studies\MSc\carmi_thesis\research\data\kibbutzim\haplotypes_ped\kib.chr.22.sample")
    x = convert_shapeit_to_rich_format(h, s)
    return x


def tryouttest_mp():
    process_all_shapeit_files(SHAPEIT_DIR, SHAPEIT_BASE_FILE_NAME, OUTPUT_DIR, chromosomes=[21, 22])


if __name__ == '__main__':
    # processed = tryouttest()
    # # In order to load csv it takes two steps: loading and then setting dtypes for the columns:
    # loaded = pd.read_csv(r"C:\Studies\MSc\carmi_thesis\research\data\kibbutzim\haplotypes_ped\kib.chr.22.csv",
    #                      header=[0, 1, 2], index_col=[0, 1, 2])
    # loaded.columns = loaded.columns.set_levels([loaded.columns.levels[0].astype(int),
    #                                             loaded.columns.levels[2].astype(int)], level=[0, 2])
    # print(np.all(processed.values == loaded.values))
    # print(processed.equals(loaded))
    # print("end")
    process_all_shapeit_files()
