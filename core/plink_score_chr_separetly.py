import subprocess
import os
import re
from itertools import product
import pandas as pd

BASE_PATH = r"C:\Studies\MSc\carmi_thesis\research"
os.chdir(BASE_PATH)  # Change current working directory
DATA_DIR = os.path.join(BASE_PATH, "data")

PLINK_EXE_PATH = os.path.join(BASE_PATH, "toolsets", "plink_v1.90", "plink.exe")
# OUTPUT_DIR = os.path.join(DATA_DIR, "profiles", "by_chr")
# OUTPUT_DIR = os.path.join(DATA_DIR, "profiles", "chr8_begin_drop")
OUTPUT_DIR = os.path.join(DATA_DIR, "profiles", "emb_var_r2", "cumulative", "longevity")
GWAS = {"longevity": {"path": os.path.join(DATA_DIR, "cent_kids", "gwas_autosomal")},
        "aspis": {"path": os.path.join(DATA_DIR, "cog_ablt", "aspis_b37_qc_eur_norel")}}
PHENO = {"height": {"sum_stats_path": os.path.join(DATA_DIR, "giant_height_2018",
                                                   # "Meta-analysis_Wood_et_al+UKBiobank_2018.txt"),
                                                   # "Meta-analysis_Wood_et_al+UKBiobank_2018_clumped.txt"),
                                                   "Meta-analysis_Wood_et_al+UKBiobank_2018_clumped2.txt"),
                    "sum_stats_cols": ["CHR", "SNP", "Tested_Allele", "BETA"]},
         "cog": {"sum_stats_path": os.path.join(DATA_DIR, "cog_ablt", "iq.edutrait_mtag.assoc"),
                 "sum_stats_cols": ["CHR", "SNP", "A1", "mtag_beta"]}}


def score_pheno(pheno_name, sum_stats_path, sum_stats_cols, gwas_name, gwas_path, cumulative=False):
    sum_stats = pd.read_csv(sum_stats_path, delim_whitespace=True)
    chromosomes = range(1, 23)
    for chr_num in chromosomes:
        score_output_name = "{}-{}-chr{:02}".format(gwas_name, pheno_name, chr_num)

        if os.path.exists(os.path.join(OUTPUT_DIR, score_output_name + ".profile")):
            continue
        if not os.path.exists(OUTPUT_DIR):
            os.makedirs(OUTPUT_DIR)

        # Filter sum stats:
        if cumulative:
            chr_mask = sum_stats[sum_stats_cols[0]] <= chr_num
        else:
            chr_mask = sum_stats[sum_stats_cols[0]] == chr_num
        chr_sum_stats = sum_stats.loc[chr_mask, sum_stats_cols[1:]]
        chr_sum_stats.to_csv(os.path.join(OUTPUT_DIR, "tmp_sum_stats.txt"), sep="\t", index=False)

        # Score:
        subprocess.run(args=[PLINK_EXE_PATH,
                             "--bfile", gwas_path,
                             "--score", os.path.join(OUTPUT_DIR, "tmp_sum_stats.txt"),
                             "1", "2", "3",  # columns: SNP name, Major allele, beta.
                             "header",
                             "no-mean-imputation",
                             "--out", os.path.join(OUTPUT_DIR, score_output_name)])

    for file_name in os.listdir(OUTPUT_DIR):
        if not file_name.endswith(".profile"):
            os.remove(os.path.join(OUTPUT_DIR, file_name))


def load_scores(pheno_name, gwas_name):
    res = {}
    chromosomes = range(1, 23)
    for chr_num in chromosomes:
        score_file_name = "{}-{}-chr{:02}.profile".format(gwas_name, pheno_name, chr_num)
        res[chr_num] = pd.read_csv(os.path.join(OUTPUT_DIR, score_file_name),
                                   delim_whitespace=True, index_col=["FID", "IID"])["SCORE"]
    res = pd.concat(res, axis="columns")
    res.columns.name = "chr"
    return res


def load_scores_from_dir(dir_path):
    res = {}
    for score_file_name in os.listdir(dir_path):
        chr_num = re.search("chr([0-9]+)", score_file_name).group(1)
        chr_num = int(chr_num)
        res[chr_num] = pd.read_csv(os.path.join(dir_path, score_file_name),
                                   delim_whitespace=True, index_col=["FID", "IID"])["SCORE"]
    res = pd.concat(res, axis="columns")
    res.columns.name = "chr"
    return res


def score():
    for pheno_name, gwas_name in product(PHENO.keys(), GWAS.keys()):
        score_pheno(pheno_name, PHENO[pheno_name]["sum_stats_path"], PHENO[pheno_name]["sum_stats_cols"],
                    gwas_name, GWAS[gwas_name]["path"])


if __name__ == '__main__':
    # score()
    # load_scores("height", "longevity")

    score_pheno("height", PHENO["height"]["sum_stats_path"], PHENO["height"]["sum_stats_cols"],
                "longevity", GWAS["longevity"]["path"], cumulative=True)
    # score_pheno("height", PHENO["height"]["sum_stats_path"], PHENO["height"]["sum_stats_cols"],
    #             "CENT-SO_2018-07-19_20-00-06",
    #             os.path.join(DATA_DIR, "cent_kids", "offspring_simulations", "CENT-SO_2018-07-19_20-00-06"),
    #             cumulative=True)
    # score_pheno("height", PHENO["height"]["sum_stats_path"], PHENO["height"]["sum_stats_cols"],
    #             "CENT-SO_2018-07-19_19-23-33",
    #             os.path.join(DATA_DIR, "cent_kids", "offspring_simulations", "CENT-SO_2018-07-19_19-23-33"),
    #             cumulative=True)
    # score_pheno("height", PHENO["height"]["sum_stats_path"], PHENO["height"]["sum_stats_cols"],
    #             "CENT-SO_2018-08-23_09-49-27",
    #             os.path.join(DATA_DIR, "cent_kids", "offspring_simulations", "CENT-SO_2018-08-23_09-49-27"),
    #             cumulative=True)
    # score_pheno("height", PHENO["height"]["sum_stats_path"], PHENO["height"]["sum_stats_cols"],
    #             "CENT-SO_2018-11-17_10-00-02",
    #             os.path.join(DATA_DIR, "cent_kids", "offspring_simulations", "CENT-SO_2018-11-17_10-00-02"),
    #             cumulative=True)
    score_pheno("height", PHENO["height"]["sum_stats_path"], PHENO["height"]["sum_stats_cols"],
                "CENT-SO_2018-11-17_10-00-02",
                os.path.join(DATA_DIR, "cent_kids", "offspring_simulations", "CENT-SO_2018-11-17_10-00-02"),
                cumulative=True)


