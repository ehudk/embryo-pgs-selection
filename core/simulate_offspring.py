import pandas as pd
import numpy as np
import os
import time
import multiprocessing
from scipy.interpolate import interp1d

# HAPLOTYPE_BASE_FILE_NAME = "gwas_autosomal_imputed.chr{:02}.{}"
# HAPLOTYPE_BASE_FILE_NAME = "gwas_autosomal.chr{:02}.{}"
# HAPLOTYPE_BASE_FILE_NAME = "kib.chr.{}.{}"
HAPLOTYPE_BASE_FILE_NAME = "aspis_b37_qc_eur_norel.chr{}.{}"
# CM_BIM = "gwas_autosomal.bim"
# CM_BIM = "kib.cm.bim"
# CM_BIM = "aspis_b37_qc_eur_norel.bim"
if os.name == "nt":
    # DATA_DIR = os.path.join("..", "data", "cent_kids")
    # DATA_DIR = os.path.join("..", "data", "kibbutzim")
    DATA_DIR = os.path.join("..", "data", "cog_ablt")
elif os.name == "posix":
    # DATA_DIR = os.path.join(os.path.sep, "cs", "icore", "ehudk", "carmi_thesis", "data", "cent_kids")
    # DATA_DIR = os.path.join(os.path.sep, "cs", "icore", "ehudk", "carmi_thesis", "data", "kibbutzim")
    DATA_DIR = os.path.join(os.path.sep, "cs", "icore", "ehudk", "carmi_thesis", "data", "cog_ablt")
else:
    raise EnvironmentError
GENETIC_MAP_BASE_PATH = os.path.join(DATA_DIR, "..", "genetic_map_b37", "genetic_map_chr{}_combined_b37.txt")
# HAPLOTYPE_DIR_PATH = os.path.join(DATA_DIR, "haplotypes")
HAPLOTYPE_DIR_PATH = os.path.join(DATA_DIR, "haplotypes_bed")
OUTPUT_DIR_PATH = os.path.join(DATA_DIR, "offspring_simulations")
SAVE_EVERY_N_OFFSPRING = 50


def simulate_offsprings(g1, g2, gmap, num_of_offsprings=1, freq=None, to_reseed=True):
    """
    Simulates offspring from two given parents.

    Parameters
    ----------
    g1 : pd.DataFrame[str]
        A two-column DataFrame, representing a parental genotype.
        Each column represent a chromatid and the index is the SNP name.
    g2 : pd.DataFrame[str]
        A two-column DataFrame, representing a parental genotype.
        Each column represent a chromatid and the index is the SNP name.
    gmap : pd.DataFrame
        A genetic map - a list of every SNP (at least the ones present in the data).
        Assumes map file derived from PLINK:
        | chr | snp | m_coo | bp_coo |
         * https://www.cog-genomics.org/plink/1.9/formats#map
         * http://zzz.bwh.harvard.edu/plink/data.shtml#map
    num_of_offsprings : int
        The number of offspring to generate.
    freq : pd.Series[float]
        Frequencies of every each minor allele. Index is SNP name and value is the frequency of the minor allele in
        that variant. This is used to infer missing alleles if found. In case not provided no inferring is done.
    to_reseed: True
        Whether to call np.random.seed() every time the function is called.
        This is a must when using multiprocess, since the workers are spawned with the same random-seed, thus creating
        identical offspring.

    Returns
    -------
    list[pd.DataFrame[str]]
        A list the size of num_of_offsprings containing two-column DataFrames, representing the simulated
        genotypes.
    """
    pool = multiprocessing.Pool()
    # TODO: Do reseeding by generating num_of_offspring random ints and then passing one to each offspring?
    arguments = zip([g1] * num_of_offsprings, [g2] * num_of_offsprings, [gmap] * num_of_offsprings,
                    [freq] * num_of_offsprings, [to_reseed] * num_of_offsprings)
    offsprings = pool.starmap(simulate_offspring, arguments)
    pool.close()
    pool.join()
    return offsprings
    # offsprings = []
    # for i in range(num_of_offsprings):
    #     offspring = simulate_offspring(g1, g2, gmap, freq, random_state)
    #     offsprings.append(offspring)
    # return offsprings


def simulate_offspring(g1, g2, gmap, freq=None, to_reseed=True):
    """
    Simulates one offspring from two given parents.

    Parameters
    ----------
    g1 : pd.DataFrame[str]
        A two-column DataFrame, representing a parental haplotypes.
        Each column represent a chromatid and the index is the SNP name.
    g2 : pd.DataFrame[str]
        A two-column DataFrame, representing a parental haplotypes.
        Each column represent a chromatid and the index is the SNP name.
    gmap : pd.DataFrame
        A genetic map - a list of every SNP (at least the ones present in the data).
        Assumes map file derived from PLINK:
        | chr | snp | m_coo | bp_coo |
         * https://www.cog-genomics.org/plink/1.9/formats#map
         * http://zzz.bwh.harvard.edu/plink/data.shtml#map
    freq : pd.Series[float]
        Frequencies of every each minor allele. Index is SNP name and value is the frequency of the minor allele in
        that variant. This is used to infer missing alleles if found. In case not provided no inferring is done.
    to_reseed: True
        Whether to call np.random.seed() every time the function is called.
        This is a must when using multiprocess, since the workers are spawned with the same random-seed, thus creating
        identical offspring.

    Returns
    -------
    A two-column DataFrame, representing the simulated haplotypes.
    """
    print("\tSimulate one offspring")
    if to_reseed:
        np.random.seed()
    offspring_chromosomes = {}      # dictionary with chromosome_number as key will be more robust to multi-process

    # 1-22 -> autosomes;    23 -> X;    24 -> Y;    25 -> pseudo-autosomal  X; 26 -> Mitochondrial.
    for chr_num in range(1, 23):
        # extract chromosome:
        c1 = g1.xs(key=chr_num, level="chr")
        c1 = c1.loc[~c1.index.duplicated(keep='first')].dropna()     # 'c' for chromosome
        c2 = g2.xs(key=chr_num, level="chr")
        c2 = c2.loc[~c2.index.duplicated(keep='first')].dropna()
        cmap = gmap.loc[gmap["chr"] == chr_num]
        cmap = cmap.set_index(c1.index.names)["cm"]

        if (set(cmap.index) != set(c1.index)) or (set(cmap.index) != set(c2.index)):
            mutual_snps = c1.index.intersection(c2.index).intersection(cmap.index)
            c1 = c1.loc[mutual_snps]
            c2 = c2.loc[mutual_snps]
            cmap = cmap.loc[mutual_snps]

        # cross-overs:      # TODO: choose method for crossover - Poisson-process or crossover-interference
        c1_co = poisson_process_crossover(c1, cmap)    # 'co' for crossover
        c2_co = poisson_process_crossover(c2, cmap)    # 'co' for crossover

        # mendelian:
        offspring_chromosome = mendelian_transmission(c1_co, c2_co)
        offspring_chromosomes[chr_num] = offspring_chromosome
    # TODO: account for sex chromosomes:
    genotype = concatenate_chromosomes(offspring_chromosomes)
    return genotype


def concatenate_chromosomes(offspring_chromosomes):
    """
    Concatenate separated haplotypes of different chromosomes into one haplotypes.

    Parameters
    ----------
    offspring_chromosomes : dict[int, pd.DataFrame]
        Dictionary which keys are chromosome number and values are two-column-DataFrames, a column for each chromatid.

    Returns
    -------
    pd.DataFrame
        A genotype concatenated from the different chromosomes.
    """
    genotype = pd.concat(offspring_chromosomes.values(), axis="index",
                         keys=offspring_chromosomes.keys(), names=["chr"])
    genotype = genotype.sort_index(axis="index", level="chr", ascending=True)
    genotype.columns.name = "HAP"
    return genotype


def mendelian_transmission(chr1, chr2):
    """
    Simulates a Mendelian process of genetic inheritance from two homologous chromosomes.

    Parameters
    ----------
    chr1 : pd.DataFrame[str]
        A two-column DataFrame, representing one parental haplotypes.
        Each column represent a chromatid and the index is the SNP name.
    chr2 : pd.DataFrame[str]
        A two-column DataFrame, representing one parental haplotypes.
        Each column represent a chromatid and the index is the SNP name.
    Returns
    -------
    pd.DataFrame
        An offspring's chromosome sampled from the parental genotypes.
    """
    chosen_chromatid_1 = chr1.sample(n=1, axis="columns")
    chosen_chromatid_2 = chr2.sample(n=1, axis="columns")
    chromosome = pd.concat([chosen_chromatid_1, chosen_chromatid_2], axis="columns", join="inner")
    chromosome.columns = [0, 1]     # to avoid two-identical named columns, if selected chromatids are both 0 or both 1
    return chromosome


def poisson_process_crossover(c, cmap):
    """
    Simulates crossovers along one chromosome using Poisson Process in an efficient way.
    First, the number of crossovers in the chromosomes is determined by the chromosome's length (in centi-Morgans).
    Second, locations of crossover events are chosen uniformly along the chromosomes (again, viewing the chromosome
    in cM units).

    Parameters
    ----------
    c : pd.DataFrame[str]
        A two-column DataFrame, each column represents one parental haplotype and he index is the SNP name.
    cmap : pd.Series
        Genetic mapping of the current chromosome stating for each SNP (index) it's coordinates in centi-Morgans.
        This is used to determine both chromosome length (needed for the number of crossover events and
        for the distance between two consecutive SNPs, determining the probability for crossover between them).
        Sliced from a map file: https://www.cog-genomics.org/plink/1.9/formats#map

    Returns
    -------
    pd.DataFrame[str]
        A two-column DataFrame, each column represents one parental haplotype resulted after genetic recombination,
        and the index is the SNP names.
    """
    # Definitions:
    # * coord (i.e. coordinates) specify the location on the true chromosome
    # * snp_iloc specify the snp in our data, and it's relevant iloc in the pandas Series

    c = c.copy()                                      # type: pd.Series

    c_length = cmap.max()
    num_of_crossovers = np.random.poisson(lam=c_length / 100, size=1)[0]   # lambda parameter from cM to Morgans
    # TODO: number of crossovers that account for sex, age (chromosome number) as well?

    # Uniformly sample the coordinates (using chromosomal distance) for crossover along the chromosome:
    crossover_coord = np.random.uniform(low=0, high=c_length, size=num_of_crossovers)
    # Convert them locations into intervals: (each will be taken from a different chromatid)
    crossover_coord = np.sort(np.append([0, c_length], crossover_coord))    # add the edges the interval "bins"
    crossover_coord_bins = [(crossover_coord[i], crossover_coord[i+1]) for i in range(len(crossover_coord) - 1)]
    # so to alternate swapping, otherwise, swapping each bin means simply swapping entire the chromosome:
    crossover_coord_bins = [crossover_coord_bins[i] for i in range(len(crossover_coord_bins)) if i % 2 == 0]

    # Execute crossovers:
    for left_edge_coord, right_edge_coord in crossover_coord_bins:
        # Find the SNP in the data closest to the coordinate in the chromosome that was drawn.
        segment_snps = np.where((cmap >= left_edge_coord) & (cmap < right_edge_coord))[0]
        if segment_snps.size == 0:
            continue

        left_edge_snp_iloc = segment_snps.min()
        right_edge_snp_iloc = segment_snps.max() + 1   # Since it later slices a [x,y) segment

        if left_edge_snp_iloc == right_edge_snp_iloc:
            continue    # the crossover event is in between two present snps, so there's no snps in data to grab & swap.

        # Swap genetic content between chromatids:
        tmp = c.iloc[left_edge_snp_iloc:right_edge_snp_iloc, 0].copy()
        c.iloc[left_edge_snp_iloc:right_edge_snp_iloc, 0] = c.iloc[left_edge_snp_iloc:right_edge_snp_iloc, 1]
        c.iloc[left_edge_snp_iloc:right_edge_snp_iloc, 1] = tmp.values
    return c


def mate(pool, method="random"):
    """
    Choose two subjects two mate from a pool.

    Parameters
    ----------
    pool : pd.DataFrame
    method: str
        what methods/model should be used to match two subjects from the pool of subjects.

    Returns
    -------
        tuple[Any, Any]: IDs of the two chosen subjects
    """
    # FIXME: adjust for a standard input
    if method == "random":
        chosen = pool.sample(n=2, axis="index")
        parent_a = tuple(chosen.iloc[0].values)
        parent_b = tuple(chosen.iloc[1].values)
    elif method == "aspis_split_to_moms_dads":
        ids = pool.reset_index()["IID"]
        parent_a = pool.loc[ids < ids.median()].sample(n=1, axis="index").iloc[0]
        parent_a = tuple(parent_a.values)
        parent_b = pool.loc[ids >= ids.median()].sample(n=1, axis="index").iloc[0]
        parent_b = tuple(parent_b.values)
    else:
        raise KeyError("The method {} is not supported".format(method))
    return parent_a, parent_b


def load_two_subjects_haplotype_profile(subject_a_idx, subject_b_idx, haplotype_dir=HAPLOTYPE_DIR_PATH, chromosomes=22):
    """
    Given an index of subject, load it's entire haplotype (for all chromosomes)

    Parameters
    ----------
    subject_a_idx:
        Subject ID to slice the rich haplotype format
    subject_b_idx:
        Subject ID to slice the rich haplotype format
    haplotype_dir: str
        Path to directory with rich-format haplotype files.
    chromosomes : int or list
        What chromosomes to load. Zero based count.

    Returns
    -------
    pd.DataFrame
    """
    if np.isscalar(chromosomes):
        chromosomes = range(1, chromosomes + 1)

    subject_a_hap_profile = []
    subject_b_hap_profile = []
    for chr_num in chromosomes:
        # # Sniff to see what cols is the data:
        # header = pd.read_csv(HAPLOTYPE_BASE_FILE_NAME.format(chr_num, "csv"),
        #                      header=[0, 1, 2], index_col=[0, 1, 2])
        # header.columns = header.columns.set_levels([header.columns.levels[0].astype(int),
        #                                             header.columns.levels[2].astype(int)], level=[0, 2])

        # Load the entire columns
        hap_file_path = os.path.join(haplotype_dir, HAPLOTYPE_BASE_FILE_NAME.format(chr_num, "pkl"))
        subject_chr_data = pd.read_pickle(hap_file_path)
        subject_a_chr_data = subject_chr_data.xs(subject_a_idx, axis="columns", level=["FID", "IID"])
        subject_b_chr_data = subject_chr_data.xs(subject_b_idx, axis="columns", level=["FID", "IID"])
        assert subject_a_chr_data.shape[1] == 2
        assert subject_b_chr_data.shape[1] == 2
        assert 0 in subject_a_chr_data.columns and 1 in subject_a_chr_data.columns
        assert 0 in subject_b_chr_data.columns and 1 in subject_b_chr_data.columns
        subject_a_hap_profile.append(subject_a_chr_data)
        subject_b_hap_profile.append(subject_b_chr_data)

    subject_a_hap_profile = pd.concat(subject_a_hap_profile, axis="index")
    subject_b_hap_profile = pd.concat(subject_b_hap_profile, axis="index")
    assert subject_a_hap_profile.shape[1] == 2
    assert subject_b_hap_profile.shape[1] == 2
    return subject_a_hap_profile, subject_b_hap_profile


def convert_rich_format_to_plink(offspring, gmap, output_path=""):
    print(output_path)
    # fam:
    print("\tCreate fam file")
    fam = offspring.columns.to_frame()[["FID", "IID"]]
    parents = fam["FID"].str.split("_", 1, expand=True).rename(columns={0: "Father", 1: "Mother"})  # maxsplit=1
    fam = fam.join(parents)
    fam = fam.drop_duplicates(subset=["FID", "IID"])  # Since each subject appears twice - one for each haplotype
    fam["Sex"] = 0  # Unknown
    fam["Phenotype"] = -9  # Unknown

    # map:
    print("\tCreate map file")
    bim_map = offspring.index.to_frame()
    # bim_map = offspring.index.to_frame().reset_index(drop=True)
    bim_map = pd.merge(bim_map, gmap, how="inner", on=["chr", "snp", "bp"])            # keep only mutually present snps
    bim_map = bim_map[["chr", "snp", "cm", "bp"]]  # rearrange column order.
    bim_map = bim_map.drop_duplicates(subset=["chr", "snp", "bp"], keep="first")

    # tped:
    print("\tCreate tped file")
    offspring.columns = range(offspring.shape[1])
    offspring = offspring.reset_index()
    offspring = offspring.drop_duplicates(subset=["chr", "snp", "bp"], keep="first")
    assert set(offspring["snp"]) == set(bim_map["snp"])  # all(offspring["snp"].values == bim_map["snp"].values)
    assert set(offspring["bp"]) == set(bim_map["bp"])
    tped = pd.merge(bim_map, offspring, on=["chr", "snp", "bp"], how="left")
    if (tped.shape[0] != offspring.shape[0]) or (tped.shape[0] != bim_map.shape[0]):
        tped = tped.drop_duplicates()
    assert tped.shape[0] == offspring.shape[0]
    assert tped.shape[0] == bim_map.shape[0]

    if output_path:
        output_dir_path = os.path.split(output_path)[0]
        if not os.path.exists(output_dir_path):
            os.makedirs(output_dir_path)
        print("\tSave fam file")
        fam.to_csv(output_path + ".tfam", sep=" ", header=False, index=False)
        print("\tSave map file")
        bim_map.to_csv(output_path + ".map", sep=" ", header=False, index=False)
        print("\tSave tped file")
        tped.to_csv(output_path + ".tped", sep=" ", header=False, index=False)
        # print("\tSave pkl file")
        # offspring.to_pickle(output_path + ".pkl")     # TODO: uncomment, but might cause crashes.
    return tped, bim_map, fam


def load_genetic_map():
    print("Loads genetic map")
    gmap = []
    for chr_num in range(1, 23):
        print("Chromosome {:02}: Loads genetic map".format(chr_num))
        cmap = pd.read_csv(GENETIC_MAP_BASE_PATH.format(chr_num), sep=" ", names=["bp", "cmmb", "cm"], skiprows=1)

        map_table = pd.read_pickle(os.path.join(HAPLOTYPE_DIR_PATH, HAPLOTYPE_BASE_FILE_NAME.format(chr_num, "pkl")))
        map_table = map_table.index.to_frame().reset_index(drop=True)
        map_table = map_table.drop_duplicates()

        f = interp1d(cmap["bp"].values, cmap["cm"].values, "slinear", fill_value="extrapolate")
        interp_cm = f(map_table["bp"].values)
        interp_cm[interp_cm <= 0] = 1e-6  # avoid negative values. but 0 means missing, so go small.
        map_table["cm"] = interp_cm
        map_table = map_table[["chr", "snp", "cm", "bp"]]   # reorder columns

        gmap.append(map_table)
    print("Concatenating genetic map")
    gmap = pd.concat(gmap, axis="index", ignore_index=True)
    return gmap


def main(number_of_mating=1, num_of_offspring=1):
    print("Started")
    gmap = load_genetic_map()

    # load parents (with gender and other phenotypes to later match upon)
    print("Loads subjects")
    subjects = load_pool_of_subjects()

    if number_of_mating == 0:
        print("Actual partnership mating")
        # Keep only families with exactly two members (couples)
        matings = subjects.groupby("FID").filter(lambda x: x.shape[0] == 2)
        # create tuples of ((fid, iid1), (fid, iid2)) based on the same fid (actual partnership):
        matings = [((fid, data["IID"].iloc[0]), (fid, data["IID"].iloc[1])) for fid, data in matings.groupby("FID")]
    elif number_of_mating > 0:
        print("Random mating")
        matings = [mate(subjects, "random") for _ in range(number_of_mating)]
    elif number_of_mating == -1:
        print("Custom mating")
        # mating_table_name_to_use = "subject_ids_for_simulation-am_height_positive.tsv"
        mating_table_name_to_use = "subject_ids_for_simulation-am_height_negative.tsv"
        # mating_table_name_to_use = "subject_ids_for_simulation-am_pheno_height_positive.tsv"
        # mating_table_name_to_use = "subject_ids_for_simulation-am_pheno_height_negative.tsv"
        matings = pd.read_csv(os.path.join(DATA_DIR, mating_table_name_to_use),
                              sep="\t", header=None, converters={0: eval, 1: eval})
        # The file name should be replaced corresponding to the mating strategy.
        matings = list(zip(matings[0], matings[1]))
    else:
        raise ValueError("number of matings parameter {} of type {} is not supported".format(number_of_mating,
                                                                                             type(number_of_mating)))
    run_name = time.strftime("ASPIS-SO_%Y-%m-%d_%H-%M-%S", time.gmtime())
    # run_name = time.strftime("KIB-SO_%Y-%m-%d_%H-%M-%S", time.gmtime())
    # run_name = time.strftime("CENT-SO_%Y-%m-%d_%H-%M-%S_{}", time.gmtime())
    print("Output name: {}".format(run_name))
    offsprings = {}
    offsprings_mini_batch = {}  # will accumulate SAVE_EVERY_N offsprings at a time
    for i, (parent_a_idx, parent_b_idx) in enumerate(matings):
        print("Mate {:03}".format(i))
        # load their haplotypes
        # # (maybe in a two step approach - first load the header of the haplotypes, find the integer number of
        # # the column they are placed in and then load the haplotypes using only these (usecols)
        # built a full profile (entire genotype?) for the parents
        try:
            print("Mate {:03}: Loads parents".format(i))
            parent_a, parent_b = load_two_subjects_haplotype_profile(parent_a_idx, parent_b_idx)
        except KeyError as e:
            print(e)
            print(e.with_traceback(None))
            continue
        # send for simulations
        print("Mate {:03}: Simulate".format(i))
        cur_offsprings = simulate_offsprings(parent_a, parent_b, gmap, num_of_offsprings=num_of_offspring)
        # the offspring IID is just the sequential:
        cur_offsprings = pd.concat(cur_offsprings, axis="columns",
                                   keys=["SO{i:02d}".format(i=j) for j in range(len(cur_offsprings))], names=["IID"])

        # Make the parents' IID the new offspring FID
        cur_fid = str(parent_a_idx[1]) + "_" + str(parent_b_idx[1])
        offsprings[cur_fid] = cur_offsprings
        offsprings_mini_batch[cur_fid] = cur_offsprings
        if i > 0 and (i + 1) % SAVE_EVERY_N_OFFSPRING == 0:   # Save minibatch of offspring
            print("Saves offspring batch {}".format(i // SAVE_EVERY_N_OFFSPRING))
            offsprings_mini_batch = pd.concat(offsprings_mini_batch, axis="columns", names=["FID"])
            batch_run_name = run_name.format(i // SAVE_EVERY_N_OFFSPRING)
            convert_rich_format_to_plink(offsprings_mini_batch, gmap, os.path.join(OUTPUT_DIR_PATH, batch_run_name))
            offsprings_mini_batch = {}

    print("Concatenate ALL offspring generated")
    offsprings = pd.concat(offsprings, axis="columns", names=["FID"])

    print("Go save results", run_name)
    tped, bim_map, fam = convert_rich_format_to_plink(offsprings, gmap, os.path.join(OUTPUT_DIR_PATH,
                                                                                     run_name.rsplit("_", 1)[0]))
    print("End")
    return offsprings, (tped, bim_map, fam)


def load_pool_of_subjects():
    # # FAM file based:
    # subjects = pd.read_csv(r"C:\Studies\MSc\carmi_thesis\research\data\kibbutzim\Kib_no_dup_1336_wME.fam",
    #                        usecols=[0, 1], header=None, names=["FID", "IID"],
    #                        delim_whitespace=True)

    # # Genotypes file based:
    subjects_hap = pd.read_pickle(os.path.join(HAPLOTYPE_DIR_PATH,
                                               HAPLOTYPE_BASE_FILE_NAME.format(22, "pkl")))  # 22 shortest -> fastest
    subjects_hap = subjects_hap.columns.to_frame().reset_index(drop=True).drop("HAP", axis="columns").drop_duplicates()

    # # For longevity:
    # subjects_filtered = pd.read_csv(os.path.join(DATA_DIR, "subject_ids_for_simulation.csv"))

    # # For ASPIS: Phenotype file based:     (loading only people with no missing phenotype)
    subjects_filtered = pd.read_csv(os.path.join(DATA_DIR, "aspis_phe_gcf_plink1"),
                                    sep=" ", usecols=[0, 1], skiprows=1, names=["FID", "IID"])

    subjects_merged = subjects_hap.merge(subjects_filtered, how="inner", on=["FID", "IID"])
    return subjects_merged


def test():
    snps = pd.Series(["100", "150", "250", "600", "1000", "15000"])
    cm = pd.Series([60, 155, 200, 76, 350, 400])
    chr_num = pd.Series([1, 1, 1, 2, 2, 2])
    g11 = pd.Series(["A", "T", "T", "C", "A", "G"], index=snps)
    g12 = pd.Series(["G", "T", "C", "C", "G", "A"], index=snps)
    g21 = pd.Series(["A", "C", "C", "C", "G", "A"], index=snps)
    g22 = pd.Series(["G", "C", "T", "G", "A", "G"], index=snps)
    gmap = pd.concat([chr_num, snps, cm], axis="columns", keys=["chr", "snp", "m_coo"])     # type: pd.DataFrame
    g1 = pd.concat([g11, g12], axis="columns", keys=[0, 1])                                 # type: pd.DataFrame
    g2 = pd.concat([g21, g22], axis="columns", keys=[0, 1])                                 # type: pd.DataFrame
    result = simulate_offspring(g1, g2, gmap)

    print("Parent 1, Parent 2")
    print(pd.concat([g1, g2], axis="columns"))
    print("Offspring")
    print(result)
    print("End test")


if __name__ == '__main__':
    # test()

    # # NOTE:
    # # -----
    # # number_of_mating can be used for different couple selection strategies (random, actual, custom)

    # offspring_all, plink_files = main(num_of_offspring=2, number_of_mating=1)
    offspring_all, plink_files = main(num_of_offspring=10, number_of_mating=500)
    # offspring_all, plink_files = main(num_of_offspring=10, number_of_mating=0)
    # offspring_all, plink_files = main(num_of_offspring=50, number_of_mating=0)
    # offspring_all, plink_files = main(num_of_offspring=50, number_of_mating=100)
    # offspring_all, plink_files = main(num_of_offspring=10, number_of_mating=-1)
    # offspring_all, plink_files = main(num_of_offspring=2, number_of_mating=-1)
    print("end")
