import statsmodels.api as sm
from sklearn.model_selection import GroupKFold
from sklearn.metrics import r2_score, mean_squared_error
import pandas as pd
import numpy as np

import os
import subprocess
from itertools import product

BASE_PATH = r"C:\Studies\MSc\carmi_thesis\research"
PLINK_EXE_PATH = os.path.join(BASE_PATH, "toolsets", "plink_v1.90", "plink.exe")


def r2_score_out_of_bag(y_valid, y_pred, y_train):
    # R-squared calculation using the mean (a model by itself) that was learned from the training too:
    valid_score_numerator = (y_valid - y_pred).pow(2).sum()
    valid_score_denominator = (y_valid - y_train.mean()).pow(2).sum()
    valid_score = valid_score_numerator / valid_score_denominator
    return valid_score


def evaluate_single_prediction(y_true, y_pred, y_true_train=None):
    if y_true_train is None:
        r2 = r2_score(y_true, y_pred)
    else:
        r2 = r2_score_out_of_bag(y_true, y_pred, y_true_train)
    scores = {"mse": mean_squared_error(y_true, y_pred),
              "r2": r2}
    return pd.Series(scores)


def evaluate_cv(X, z, y, n_splits=10, score_out_of_bag=False):
    train_scores, valid_scores = [], []
    models = []

    if n_splits > 0:
        kfold = GroupKFold(n_splits=n_splits)
        kfold_splits = kfold.split(X=X, y=y, groups=X.index.get_level_values(level="FID"))
    else:
        # Test only on train set: By making one fold with the entire data in it.
        # Note that validation set is redundant here, but only to making it compatible
        kfold_splits = [(np.arange(X.shape[0]), np.arange(X.shape[0]))]

    if not X.empty:
        # Fit one covariate model and do downstream analysis on the residuals:
        X = sm.add_constant(X)
        covariate_model = sm.OLS(endog=y, exog=X).fit()
        y = covariate_model.resid
        models.append(covariate_model)
    z = sm.add_constant(z)

    for train_idx, valid_idx in kfold_splits:
        z_train, z_valid = z.iloc[train_idx, :], z.iloc[valid_idx, :]
        y_train, y_valid = y.iloc[train_idx], y.iloc[valid_idx]

        pgs_model = sm.OLS(endog=y_train, exog=z_train).fit()

        train_score = evaluate_single_prediction(y_train, pgs_model.predict(z_train))
        valid_score = evaluate_single_prediction(y_valid, pgs_model.predict(z_valid),
                                                 y_true_train=y_valid if score_out_of_bag else None)

        train_scores.append(train_score)
        valid_scores.append(valid_score)
        models.append(pgs_model)

    # Score on entire train set:
    pgs_model = sm.OLS(endog=y, exog=z).fit()
    models.append(pgs_model)
    all_score = evaluate_single_prediction(y, pgs_model.predict(z))

    scores = pd.concat({"train": pd.DataFrame(train_scores), "valid": pd.DataFrame(valid_scores),
                        "all": all_score.to_frame().T},
                       axis="index", names=["phase", "fold"])
    scores.columns.name = "metric"
    return scores


def grid_search(X, y, gwas_path, sum_stats, output_dir, save_filtered_sumstats=False, clump_gwas_path=None):
    """

    Parameters
    ----------
    gwas_path : str
    sum_stats: pd.DataFrame
        1st column - SNP name
        2nd column - effect allele
        3rd column - effect size (beta)
        4th column - p-value
    clump_gwas_path : str
        GWAS used for clumping. if None, uses the same gwas as for scoring.

    Returns
    -------

    """
    clump_gwas_path = gwas_path if clump_gwas_path is None else clump_gwas_path
    filtered_sum_stats_tmp_path = os.path.join(output_dir, "filtered_sum_stats_tmp.tsv")
    scores = {}
    # p_vals = np.sort(np.append(10.0 ** -np.arange(7), 5 * 10.0 ** -np.arange(7)))[::-1][1:]  # to remove 5.0
    p_vals = np.sort(10.0 ** -np.arange(7))[::-1]
    p_vals = [0.1]
    # p_vals = np.sort(np.append(10.0 ** -np.arange(7), np.linspace(0, 1, 11)[2:-1]))[::-1]  # tighter grid on 0.1 - 0.9
    r2_vals = np.arange(0.0, 1.0, 0.1)[::-1]
    # r2_vals = [0.2, 0.3, 0.4]
    r2_vals = [0.1]

    for p_val, r2_val in product(p_vals, r2_vals):
        output_score_path = os.path.join(output_dir, "pgs_p={}_r2={}".format(p_val, r2_val))

        if not os.path.exists(output_score_path + ".profile"):
            filtered_sum_stats = sum_stats.copy()                                               # type: pd.DataFrame

            # P-value filter sumstats:
            if p_val is not None:
                filtered_sum_stats = filtered_sum_stats.loc[filtered_sum_stats.iloc[:, 3] <= p_val]
            filtered_sum_stats.to_csv(filtered_sum_stats_tmp_path, sep="\t", index=False)

            # LD-pruning sumstats:
            if r2_val is not None:
                subprocess.run(args=[PLINK_EXE_PATH,
                                     "--bfile", clump_gwas_path,
                                     "--clump", filtered_sum_stats_tmp_path,
                                     "--clump-p1", "1",             # Default
                                     "--clump-p2", "1",             # Default
                                     "--clump-r2", str(r2_val),
                                     "--clump-kb", "250",           # Default
                                     "--clump-snp-field", filtered_sum_stats.columns[0],
                                     "--clump-field", filtered_sum_stats.columns[3],
                                     "--out", filtered_sum_stats_tmp_path])
                clumped_snps = pd.read_csv(filtered_sum_stats_tmp_path + ".clumped",
                                           delim_whitespace=True, usecols=["SNP", "P"],
                                           dtype={"SNP": str, "P": np.float64})
                clumped_snps = filtered_sum_stats.iloc[:, 0].isin(clumped_snps["SNP"])
                filtered_sum_stats = filtered_sum_stats.loc[clumped_snps, :]
                filtered_sum_stats.to_csv(filtered_sum_stats_tmp_path, sep="\t", index=False)

            # PLINK score
            subprocess.run(args=[PLINK_EXE_PATH,
                                 "--bfile", gwas_path,
                                 "--score", filtered_sum_stats_tmp_path,
                                 "1", "2", "3",  # columns: SNP name, Major allele, beta.
                                 "header",
                                 "no-mean-imputation",
                                 "--out", output_score_path])

            if save_filtered_sumstats:
                filtered_sum_stats.to_csv(output_score_path[3:] + "_sumstats.tsv",
                                          sep="\t", index=False)

        # Load PGS:
        pgs = pd.read_csv(output_score_path + ".profile", delim_whitespace=True,
                          usecols=["FID", "IID", "SCORE"], index_col=["FID", "IID"])
        pgs = pgs.loc[X.index]

        # Evaluate prediction:
        score = evaluate_cv(X, pgs, y)
        scores[(p_val, r2_val)] = score

    scores = pd.concat(scores, axis="columns", names=["p_val", "r2"]).stack(["p_val", "r2"])
    for tmp_file in [filtered_sum_stats_tmp_path, filtered_sum_stats_tmp_path + ".clumped",
                     filtered_sum_stats_tmp_path + "log"]:
        if os.path.exists(tmp_file):
            os.remove(tmp_file)
    return scores


def main():
    np.random.seed(0)
    # OUTPUT:
    # pgs_output_dir = os.path.join(BASE_PATH, "data", "profiles", "grid_search", "longevity")
    # pgs_output_dir = os.path.join(BASE_PATH, "data", "profiles", "grid_search", "longevity_clumped")
    # pgs_output_dir = os.path.join(BASE_PATH, "data", "profiles", "grid_search", "aspis")
    # pgs_output_dir = os.path.join(BASE_PATH, "data", "profiles", "grid_search", "aspis_clumped")
    # pgs_output_dir = os.path.join(BASE_PATH, "data", "profiles", "grid_search", "aspis_ukbb")
    # pgs_output_dir = os.path.join(BASE_PATH, "data", "profiles", "grid_search", "aspis_ukbb_clumped")
    # pgs_output_dir = os.path.join(BASE_PATH, "data", "profiles", "grid_search", "aspis_savage_clumped")
    # pgs_output_dir = os.path.join(BASE_PATH, "data", "profiles", "grid_search", "aspis_savage_clumped_no-AT-CG")
    pgs_output_dir = os.path.join(BASE_PATH, "data", "profiles", "grid_search", "longevity_bmi")
    if not os.path.exists(pgs_output_dir):
        os.makedirs(pgs_output_dir)

    # SUMSTATS:
    # sum_stats = load_height_sum_stats()
    # sum_stats = load_gcf_sum_stats()
    # sum_stats = load_gcf_sum_stats(ukbb=True)
    # sum_stats = load_gcf_sum_stats(savage=True)
    sum_stats = load_bmi_sum_stats()

    # DATA:
    # covar, gwas_path, pheno = load_longevity_data(phenotype="bmi")
    # covar, gwas_path, pheno = load_aspis_data()
    # covar, gwas_path, pheno = load_aspis_data(ukbb=True)
    covar, gwas_path, pheno = load_longevity_data(phenotype="bmi")

    # scores = grid_search(X=covar, y=pheno, gwas_path=gwas_path, sum_stats=sum_stats, output_dir=pgs_output_dir)
    scores = grid_search(X=covar, y=pheno, gwas_path=gwas_path, sum_stats=sum_stats, output_dir=pgs_output_dir,
                         clump_gwas_path=os.path.join(BASE_PATH, "data", "tagc_for_prune",
                                                      "tagc_filtered-by-longevity"))
    scores.to_pickle(os.path.join(pgs_output_dir, "scores.pkl"))
    print(scores)


def load_longevity_data(phenotype="height"):
    DATA_DIR = os.path.join(BASE_PATH, "data", "cent_kids")
    gwas_path = os.path.join(DATA_DIR, "gwas_autosomal")
    covar = pd.read_csv(os.path.join(DATA_DIR, "covariates.tsv"), sep="\t", keep_default_na=True,
                        index_col=["FID", "IID"], usecols=["FID", "IID", "Gender", "Age"],
                        converters={"Gender": {"M": 0, "F": 1}.get})
    if phenotype == "height":
        pheno = pd.read_csv(os.path.join(DATA_DIR, "phenotype.tsv"), sep="\t",
                            index_col=["FID", "IID"]).iloc[:, 0].rename("Height")
    elif phenotype == "bmi":
        pheno = pd.read_csv(os.path.join(DATA_DIR, "LongevityBMI.txt"), sep="\t",
                            index_col=["FID", "IID"]).iloc[:, -1].rename("BMI")
    else:
        raise ValueError("phenotype can only be 'height' or 'bmi'.")
    pheno = pheno.dropna()
    pheno = pheno.sample(frac=1.0)  # Shuffle data
    covar = covar.loc[pheno.index]
    return covar, gwas_path, pheno


def load_height_sum_stats():
    sum_stats = pd.read_csv(os.path.join(BASE_PATH, "data", "giant_height_2018",
                                         "Meta-analysis_Wood_et_al+UKBiobank_2018_2.txt"
                                         # "Meta-analysis_Wood_et_al+UKBiobank_2018_clumped-r201-p110-p210_flip.txt"
                                         ),
                            delim_whitespace=True)
    sum_stats = sum_stats[["SNP", "Tested_Allele", "BETA", "P"]]
    return sum_stats


def load_bmi_sum_stats():
    sum_stats = pd.read_csv(os.path.join(BASE_PATH, "data", "giant_bmi_2018",
                                         "Meta-analysis_Locke_et_al+UKBiobank_2018_UPDATED-flip.txt"),
                            delim_whitespace=True)
    sum_stats = sum_stats[["SNP", "Tested_Allele", "BETA", "P"]]
    return sum_stats


def load_aspis_data(ukbb=False):
    DATA_DIR = os.path.join(BASE_PATH, "data", "cog_ablt")
    if ukbb:
        gwas_path = os.path.join(DATA_DIR, "aspis_b37_qc_eur_norel_ukbb-sumstats")
    else:
        gwas_path = os.path.join(DATA_DIR, "aspis_b37_qc_eur_norel")
    pheno = pd.read_csv(os.path.join(DATA_DIR, "aspis_phe_gcf_plink1"), delim_whitespace=True, index_col=[0, 1],
                        header=None, skiprows=1, names=["FID", "IID", "gcf"])
    pheno = (pheno.iloc[:, 0] * 15 + 100).rename("IQ")
    pheno = pheno.dropna()
    pheno = pheno.sample(frac=1.0)  # Shuffle data
    covar = pd.DataFrame(index=pheno.index)
    return covar, gwas_path, pheno


def load_gcf_sum_stats(mtag=False, ukbb=False, savage=False):
    if ukbb:
        sum_stats_path = os.path.join(BASE_PATH, "data", "ukbb_iq",
                                      "20016_raw.gwas.imputed_v3.both_sexes.high_conf.1_letter_allele.pos_chr_col.tsv")
        sum_stats_cols = ["chr_pos", "minor_allele", "beta", "pval"]
    elif mtag:
        sum_stats_path = os.path.join(BASE_PATH, "data", "cog_ablt", "iq.edutrait_mtag.assoc")
        sum_stats_cols = ["SNP", "A1", "mtag_beta", "mtag_pval"]
    elif savage:
        sum_stats_path = os.path.join(BASE_PATH, "data", "savage_iq", "iq_metaanalysis_noCOGENT_beta-se.out")
        # sum_stats_path = os.path.join(BASE_PATH, "data", "savage_iq", "iq_metaanalysis_noCOGENT_beta-se_no-AT-CG.out")
        sum_stats_cols = ["SNP", "Allele1", "BETA", "P"]
    else:
        sum_stats_path = os.path.join(BASE_PATH, "data", "cog_ablt", "aspis_mtag_prs_clumped.assoc")
        sum_stats_cols = ["SNP", "A1", "BETA", "P"]
    sum_stats = pd.read_csv(sum_stats_path, usecols=sum_stats_cols, sep="\t")
    sum_stats = sum_stats[sum_stats_cols]
    return sum_stats


if __name__ == '__main__':
    main()

