# Screening human embryos for polygenic traits has limited utility

Code base accompanying the preprint *Screening human embryos for polygenic traits has limited utility* 
[[bioRxiv:626846](https://www.biorxiv.org/content/10.1101/626846v1)]

