import pandas as pd
import statsmodels.api as sm
import os
import re
from typing import Union


def load_longevity_data(pgs_col=None, convert_to_cm=False, recode_gender=True, drop_na=True, add_const_col=True):
    """

    Parameters
    ----------
    pgs_col : str | pd.DataFrame | pd.Series, optional
        if string - the location (path) to load a delim_whitespace score file (output of plink --score) and use its last
                    column.
        If data - an indexed (FID & IID) DataFrame/Series to directly concatenate to the data.
        if None - no PGS column will be added.
    convert_to_cm : book
        Whether to convert height phenotype from meters to centimeters.
    recode_gender : bool
        Whether to recode sex column from "M"&"F" to 0&1.
    drop_na : bool
        Whether to drop missing values.
    add_const_col : bool
        Whether to add constant column (for later usage in regression)

    Returns
    -------
    pd.DataFrame
        The covariates and phenotype.
    """
    data = _load_longevity_data("height", pgs_col, recode_gender, drop_na, add_const_col)
    if convert_to_cm:
        data["Height"] *= 100
    return data


def load_longevity_bmi_data(pgs_col=None, negate_bmi=False, recode_gender=True, drop_na=True, add_const_col=True):
    """

    Parameters
    ----------
    pgs_col : str | pd.DataFrame | pd.Series, optional
        if string - the location (path) to load a delim_whitespace score file (output of plink --score) and use its last
                    column.
        If data - an indexed (FID & IID) DataFrame/Series to directly concatenate to the data.
        if None - no PGS column will be added.
    negate_bmi : bool
        Whether to negate the BMI measures so that higher is better.
    recode_gender : bool
        Whether to recode sex column from "M"&"F" to 0&1.
    drop_na : bool
        Whether to drop missing values.
    add_const_col : bool
        Whether to add constant column (for later usage in regression)

    Returns
    -------
    pd.DataFrame
        The covariates and phenotype.
    """
    data = _load_longevity_data("bmi", pgs_col, recode_gender, drop_na, add_const_col)
    if negate_bmi:
        data["BMI"] *= -1
    return data


def _load_longevity_data(phenotype, pgs_col=None, recode_gender=True, drop_na=True, add_const_col=True):
    """

    Parameters
    ----------
    phenotype : str
        What phenotype to load: either "height" or "bmi".
    pgs_col : str | pd.DataFrame | pd.Series, optional
        if string - the location (path) to load a delim_whitespace score file (output of plink --score) and use its last
                    column.
        If data - an indexed (FID & IID) DataFrame/Series to directly concatenate to the data.
        if None - no PGS column will be added.
    recode_gender : bool
        Whether to recode sex column from "M"&"F" to 0&1.
    drop_na : bool
        Whether to drop missing values.
    add_const_col : bool
        Whether to add constant column (for later usage in regression)

    Returns
    -------
    pd.DataFrame
        The covariates and phenotype (in the last columns).
    """
    if isinstance(pgs_col, str):
        pgs_col = load_pgs(pgs_col)
    covariates = pd.read_csv(os.path.join("data", "longevity_covariates.tsv"), sep="\t",
                             keep_default_na=True,
                             usecols=["FID", "IID", "Gender", "Age"], index_col=["FID", "IID"])
    phenotype = pd.read_csv(os.path.join("data", f"longevity_{phenotype}.tsv"), sep="\t",
                            index_col=["FID", "IID"])
    data = pd.concat([covariates, pgs_col, phenotype], axis="columns")
    if recode_gender:
        data["Gender"] = data["Gender"].replace({"M": 1, "F": 0})
    if drop_na:
        data = data.dropna(how="any")
    if add_const_col:
        data = sm.add_constant(data)

    return data


def load_aspis_data(pgs_col=None, convert_to_iq=True, drop_na=True, add_const_col=True):
    if isinstance(pgs_col, str):
        pgs_col = load_pgs(pgs_col)

    phenotype = pd.read_csv(os.path.join(os.path.join("data", "aspis_gcf.txt")), delim_whitespace=True,
                            index_col=[0, 1], header=None, skiprows=1, names=["FID", "IID", "GCF"])

    if convert_to_iq:
        phenotype = phenotype * 15 + 100
        phenotype = phenotype.rename(columns={"GCF": "IQ"})

    data = pd.concat([pgs_col, phenotype], axis="columns")

    if drop_na:
        data = data.dropna(how="any")
    if add_const_col:
        data = sm.add_constant(data)

    return data


def load_pgs(file_path: str,
             name: Union[str, int] = "PGS"):
    if not file_path.endswith(".profile"):
        file_path += ".profile"
    pgs = pd.read_csv(file_path, delim_whitespace=True, index_col=["FID", "IID"])
    pgs = pgs.iloc[:, -1]   # in PLINK's --score output the score is last column.
    pgs = pgs.rename(name)
    return pgs


def load_pgs_by_chromosome(dir_path: str):
    res = {}
    for score_file_name in os.listdir(dir_path):
        extension = score_file_name.rsplit(".", maxsplit=1)[-1]
        if extension != "profile":
            continue
        chr_num = re.search("chr([0-9]{1,2})", score_file_name).group(1)
        chr_num = int(chr_num)
        pgs = load_pgs(os.path.join(dir_path, score_file_name), name=chr_num)
        res[chr_num] = pgs
    res = pd.concat(res, axis="columns")
    res.columns.name = "chr"
    return res


def delete_plink_generated_aux_files(output_path):
    for extension in ["nosex", "nopred", "log"]:
        file_name = output_path + "." + extension
        if os.path.exists(file_name):
            os.remove(file_name)

