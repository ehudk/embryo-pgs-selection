import numpy as np
from scipy import stats
import pandas as pd
import statsmodels.api as sm
import seaborn as sns

import os
import pickle


def calculate_mutual_bins(x, y):
    data = x.append(y, ignore_index=True)
    #     n_bins = sns.distributions._freedman_diaconis_bins(data)
    bins = np.histogram(data, bins="auto")[1]
    return bins


def calculate_smd(x, y, with_cov=True):
    # https://en.wikipedia.org/wiki/Strictly_standardized_mean_difference#Concept
    numerator = x.mean() - y.mean()
    if x.size == y.size:
        xy_cov = np.cov(x, y)[0, 1] if with_cov else 0
    else:
        xy_cov = 0
    denominator = np.sqrt(x.var() + y.var() - (2 * xy_cov))
    return numerator / denominator


def display_distribution_differences(x, y, ax=None, cumulative=True, annotate=True, ecdf=False, mutual_bins=False,
                                     xlabel=None, annotate_dist=0.2):
    if ecdf:
        x_ecdf = sm.distributions.ECDF(x)
        y_ecdf = sm.distributions.ECDF(y)
        ax.plot(x_ecdf.x, x_ecdf.y, label=x.name)
        ax.plot(y_ecdf.x, y_ecdf.y, label=y.name)
    else:
        if mutual_bins:
            bins = calculate_mutual_bins(x, y)
        else:
            bins = None
        sns.distplot(x, label=x.name, ax=ax, bins=bins,
                     hist_kws=dict(cumulative=cumulative), kde_kws=dict(cumulative=cumulative))
        sns.distplot(y, label=y.name, ax=ax, bins=bins,
                     hist_kws=dict(cumulative=cumulative), kde_kws=dict(cumulative=cumulative))
    ax.set_xlabel(xlabel)
    ax.legend()

    if annotate:
        smd = calculate_smd(x, y)
        ks = stats.ks_2samp(x, y)
        #     trans = ax.get_xaxis_transform() # x in data untis, y in axes fraction
        ax.annotate("SMD: {smd:.3f}\nKS statistic: {kss:.3f}, pval: {ksp:.3e}".format(smd=smd, kss=ks[0], ksp=ks[1]),
                    xy=(0.5, -annotate_dist), xycoords="axes fraction", ha="center", va="center")


def joint_plot(x, y, kind="reg", plot_x_eq_y=True):
    g = sns.jointplot(x=x.name, y=y.name, data=pd.concat([x, y], axis=1),
                      kind=kind)
    if plot_x_eq_y:
        # Get most minima and maxima among plot limits:
        #  x_eq_y_line = [min(min(g.ax_joint.get_xlim()), min(g.ax_joint.get_ylim())),
        #                 max(max(g.ax_joint.get_xlim()), max(g.ax_joint.get_ylim()))]
        x_eq_y_line = [max(g.ax_joint.get_xlim()[0], g.ax_joint.get_ylim()[0]),
                       min(g.ax_joint.get_xlim()[1], g.ax_joint.get_ylim()[1])]

        # plot x=y line based on the limits acheived above:
        g.ax_joint.plot(x_eq_y_line, x_eq_y_line,
                        color="black", linestyle="--", label="x=y")
        # Adjust the legend to add the "x=y" label to the plot:
        g.ax_joint.legend(handles=g.ax_joint.lines,
                          labels=[list(g.ax_joint.get_legend().get_texts())[0].get_text(), "x=y"],
                          loc="upper left")
    return g


def save_fig(fig, name, dir_path, dpi=600, types=("png", "pdf", "svg"), save_pickle=True):
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    name += ".{ext}"
    fig_path_base = os.path.join(dir_path, name)
    for extension in types:
        fig.savefig(fig_path_base.format(ext=extension), dpi=dpi, bbox_inches="tight")
    if save_pickle:
        with open(fig_path_base.format(ext="pkl"), "wb") as fh:
            pickle.dump(fig, fh)
