import os
import subprocess
import pandas as pd
from typing import List


BASE_PATH = r"C:\Studies\MSc\carmi_thesis\research"
PROJ_PATH = os.path.join(BASE_PATH, "embryo-pgs-selection", "data")
PGS_OUTPUT_DIR = os.path.join(PROJ_PATH, "pgs")
PLINK_EXE_PATH = os.path.join(BASE_PATH, "toolsets", "plink_v1.90", "plink.exe")
SUMSTATS_HEIGHT_PATH = os.path.join(PROJ_PATH, "sumstats", "height.txt")
SUMSTATS_BMI_PATH = os.path.join(PROJ_PATH, "sumstats", "bmi.tsv")
SUMSTATS_GCF_PATH = os.path.join(PROJ_PATH, "sumstats", "gcf.out")


def score_height(plink_base_file, output_name, delete_aux_output_files=True,
                 sumstats_path=SUMSTATS_HEIGHT_PATH):
    output_path = os.path.join(PGS_OUTPUT_DIR, output_name)

    # Score:
    subprocess.run(args=[PLINK_EXE_PATH,
                         "--bfile", plink_base_file,
                         "--score", sumstats_path,
                         "3", "4", "7",  # columns: SNP name, Major allele, beta.
                         "header",
                         "no-mean-imputation",
                         "--out", output_path])

    # Remove redundant PLINK-generated files which are not the scores:
    if delete_aux_output_files:
        delete_plink_generated_aux_files(output_path)
    return os.path.exists(output_path + "." + "profile")


def score_bmi(plink_base_file, output_name, delete_aux_output_files=True,
              sumstats_path=SUMSTATS_BMI_PATH):
    output_path = os.path.join(PGS_OUTPUT_DIR, output_name)

    # Score:
    subprocess.run(args=[PLINK_EXE_PATH,
                         "--bfile", plink_base_file,
                         "--score", sumstats_path,
                         "3", "4", "7",  # columns: SNP name, Major allele, beta.
                         "header",
                         "no-mean-imputation",
                         "--out", output_path])

    # Remove redundant PLINK-generated files which are not the scores:
    if delete_aux_output_files:
        delete_plink_generated_aux_files(output_path)
    return os.path.exists(output_path + "." + "profile")


def score_gcf(plink_base_file, output_name, delete_aux_output_files=True,
              sumstats_path=SUMSTATS_GCF_PATH):
    output_path = os.path.join(PGS_OUTPUT_DIR, output_name)

    # Score:
    subprocess.run(args=[PLINK_EXE_PATH,
                         "--bfile", plink_base_file,
                         "--score", sumstats_path,
                         "2", "5", "12",  # columns: SNP name, Major allele, beta.
                         "header",
                         "no-mean-imputation",
                         "--out", output_path])

    # Remove redundant PLINK-generated files which are not the scores:
    if delete_aux_output_files:
        delete_plink_generated_aux_files(output_path)
    return os.path.exists(output_path + "." + "profile")


def score_by_chromosome(gwas_path, sum_stats_path, sum_stats_headers, output_name, cumulative=False):
    """

    Parameters
    ----------
    gwas_path : str
    sum_stats_path : str
    sum_stats_headers : List
        list of summary statistic file headers (to be used with [] on a DataFrame).
         sum_stats_headers[0] - header of chromosome column
         sum_stats_headers[1] - header of SNP ID (variant name) column
         sum_stats_headers[2] - header of effect allele (test allele) column
         sum_stats_headers[3] - header of effect size (beta) column
    output_name : str
        The name of the output score.
        Will create a directory of the name output_name + "by-chr",
        And place the scores in it with yhe name output_name + "_chr{chr_num:02}"5
    cumulative : bool
        for each chromosome c, whether to score with all chromosomes up to c (1, ..., c), or only c.

    Returns
    -------

    """
    output_path = os.path.join(PGS_OUTPUT_DIR, output_name)
    output_dir = os.path.join(output_path + "_by-chr" + "-cum" if cumulative else "")
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    output_path = os.path.join(output_dir, output_name)

    sum_stats = pd.read_csv(sum_stats_path, delim_whitespace=True)
    tmp_filtered_sum_stats_path = os.path.join(PGS_OUTPUT_DIR, "tmp_sum_stats.tsv")

    chromosomes = sum_stats[sum_stats_headers[0]].unique()

    for chr_num in chromosomes:
        chr_output_path = output_path + "_chr{:02}".format(chr_num)

        if os.path.exists(chr_output_path + ".profile"):
            continue

        # Filter sum stats:
        if cumulative:
            chr_mask = sum_stats[sum_stats_headers[0]] <= chr_num
        else:
            chr_mask = sum_stats[sum_stats_headers[0]] == chr_num
        chr_sum_stats = sum_stats.loc[chr_mask, sum_stats_headers[1:]]  # extract only relevant columns for scoring
        chr_sum_stats.to_csv(tmp_filtered_sum_stats_path, sep="\t", index=False)  # save

        # Score:
        subprocess.run(args=[PLINK_EXE_PATH,
                             "--bfile", gwas_path,
                             "--score", tmp_filtered_sum_stats_path,
                             "1", "2", "3",  # columns: SNP name, Major allele, beta.
                             "header",
                             "no-mean-imputation",
                             "--out", chr_output_path])

        delete_plink_generated_aux_files(chr_output_path)

    os.remove(tmp_filtered_sum_stats_path)
    return len(os.listdir(output_dir)) == len(chromosomes)


def delete_plink_generated_aux_files(output_path):
    for extension in ["nosex", "nopred", "log"]:
        file_name = output_path + "." + extension
        if os.path.exists(file_name):
            os.remove(file_name)


if __name__ == '__main__':
    score_by_chromosome(gwas_path=r"C:\Studies\MSc\carmi_thesis\research\data\cent_kids\gwas_autosomal",
                        sum_stats_path=r"C:\Studies\MSc\carmi_thesis\research\embryo_variance\data\sumstats\height.txt",
                        sum_stats_headers=["CHR", "SNP", "Tested_Allele", "BETA"],
                        output_name="longevity_height", cumulative=True)
